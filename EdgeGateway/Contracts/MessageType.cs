﻿namespace Hsrm.EdgeGateway.Contracts
{
    public enum MessageType
    {
        CommandInvocation,
        PropertyRead,
        StateUpdate,
        IntermediateValue,
        CommandResponse
    }
}