﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Server;
using MetadataTypeAttribute = Tecan.Sila2.Server.MetadataTypeAttribute;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// This feature is used to provide generic gateway functionality
    /// </summary>
    [SilaFeature(true, "gateway")]
    public interface IGatewayService : IGatewayConfigurationService
    {
        /// <summary>
        /// This client metadata needs to be attached to every command invocation in order to specify the server on which the command must be executed
        /// </summary>
        [MetadataType(typeof(string))]
        IRequestInterceptor ServerUuid { get; }

        /// <summary>
        /// Lists the servers connected to this gateway
        /// </summary>
        [Observable]
        IReadOnlyList<Server> ConnectedServers { get; }

        /// <summary>
        /// Signifies whether a new feature has been registered that requires a server restart to become available
        /// </summary>
        bool HasNewFeature { get; }

        /// <summary>
        /// Performs a network discovery for the specified duration
        /// </summary>
        /// <param name="duration">The duration for which the gateway should look for servers</param>
        void ScanNetwork([Unit("s", Second = 1)] int duration);

        /// <summary>
        /// Restarts the gateway
        /// </summary>
        void Restart();

        /// <summary>
        /// Set the name of a connected server 
        /// </summary>
        /// <param name="serverUuid"></param>
        /// <param name="name"></param>
        void SetServerName(string serverUuid, string name);

        /// <summary>
        /// Get the identifiers of the required metadata
        /// </summary>
        /// <param name="serverUuid"></param>
        /// <param name="identifier">The fully qualified identifier of the command or property</param>
        /// <returns></returns>
        IReadOnlyList<string> GetRequiredMetadata(string serverUuid, string identifier);
    }
}
