﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    public class Server
    {
        public Server(string serverUuid, string name, string type, string version, string description, string vendorUri, IReadOnlyList<string> features)
        {
            ServerUuid = serverUuid;
            Name = name;
            Type = type;
            Version = version;
            Description = description;
            VendorUri = vendorUri;
            Features = features;
        }

        [MaximalLength(36)]
        [PatternConstraint("[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}")]
        public string ServerUuid { get; }

        public string Name { get; }

        public string Type { get; }

        public string Version { get; }

        public string Description { get; }

        public string VendorUri { get; }

        public IReadOnlyList<string> Features { get; }
    }
}
