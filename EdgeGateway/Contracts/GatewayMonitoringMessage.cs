﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    public class GatewayMonitoringMessage
    {
        public GatewayMonitoringMessage(MessageType type, string serverUuid, string identifier, DateTimeOffset time, StateUpdate stateUpdate)
        {
            Time = time;
            Type = type;
            ServerUuid = serverUuid;
            Identifier = identifier;
            StateUpdate = stateUpdate;
        }

        public GatewayMonitoringMessage(string serverUuid, string identifier, StateUpdate stateUpdate) : this(MessageType.StateUpdate, serverUuid, identifier, DateTimeOffset.Now, stateUpdate) { }

        public GatewayMonitoringMessage(MessageType type, string serverUuid, string identifier, DateTimeOffset time)
        {
            Time = time;
            Type = type;
            ServerUuid = serverUuid;
            Identifier = identifier;
        }

        public GatewayMonitoringMessage(MessageType type, string serverUuid, string identifier) : this(type, serverUuid, identifier, DateTimeOffset.Now) { }

        public GatewayMonitoringMessage() { }

        public MessageType Type { get; }

        public string ServerUuid { get; }

        public string Identifier { get; }

        public DateTimeOffset Time { get; }

        public StateUpdate StateUpdate { get; }
    }
}
