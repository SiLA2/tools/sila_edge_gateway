﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Initializes the gateway
    /// </summary>
    public interface IGatewayInitialization
    {
        /// <summary>
        /// Initializes the gateway
        /// </summary>
        void Initialize();
    }
}
