﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/gateway")]
    public class PoolController : Controller
    {
        private readonly IServerPool _serverPool;
        private readonly IServerResolver _serverResolver;

        public PoolController(IServerPool serverPool, IServerResolver serverResolver)
        {
            _serverPool = serverPool;
            _serverResolver = serverResolver;
        }

        [HttpGet]
        public IEnumerable<ServerInfo> GetConnectedServers()
        {
            var connectedServers = _serverPool.ConnectedServers ?? Enumerable.Empty<ServerData>();
            var executionManager = new DiscoveryExecutionManager();

            var result = new List<ServerInfo>();
            foreach (var server in connectedServers)
            {
                result.Add(new ServerInfo(server.Config.Uuid.ToString(), server.Config.Name, server.Info.Type, server.Info.Description, server.Features.Select(f => f.FullyQualifiedIdentifier).ToArray()));

                if (server.Features.Any(feature => feature.FullyQualifiedIdentifier == "org.silastandard/gateway/GatewayService/v1"))
                {
                    var gatewayClient = new GatewayServiceClient(server.Channel, executionManager);
                    var serversConnectedToGateway = gatewayClient.ConnectedServers ?? Enumerable.Empty<GatewayService.Server>();
                    result.AddRange(serversConnectedToGateway.Select(server => new ServerInfo(server.ServerUuid, server.Name, server.Type, server.Description, server.Features.ToArray())));
                }
            }

            return result;
        }

        public record ServerInfo(
            string serveruuid,
            string serverName,
            string serverType,
            string description,
            IReadOnlyList<string> features);
    }
}
