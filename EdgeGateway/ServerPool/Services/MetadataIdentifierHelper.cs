﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Tecan.Sila2.Discovery;

namespace Hsrm.EdgeGateway.Services
{
    public class MetadataIdentifierHelper : IMetadataIdentifierHelper
    {
        private readonly IServerResolver _serverResolver;

        public MetadataIdentifierHelper(IServerResolver serverResolver)
        {
            _serverResolver = serverResolver;
        }

        public IReadOnlyList<string> GetRequiredMetadata(string serverUuid, string identifier)
        {
            var gateway = _serverResolver.ResolveServer(serverUuid);
            var gatewayServiceClient = new GatewayServiceClient(gateway.Server.Channel, new DiscoveryExecutionManager());
            return gatewayServiceClient.GetRequiredMetadata(serverUuid, identifier).ToList();
        }
    }
}
