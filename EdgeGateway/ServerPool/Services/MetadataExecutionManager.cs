﻿using Tecan.Sila2;
using Tecan.Sila2.Client;

namespace Hsrm.EdgeGateway.Services
{
    public class MetadataExecutionManager : IClientExecutionManager
    {
        private readonly IClientExecutionManager _innerExecutionManager;
        private readonly string _serverUuid;

        public MetadataExecutionManager(IClientExecutionManager innerExecutionManager, string serverUuid)
        {
            _innerExecutionManager = innerExecutionManager;
            _serverUuid = serverUuid;
        }

        public IBinaryStore DownloadBinaryStore => _innerExecutionManager.DownloadBinaryStore;

        public IBinaryStore CreateBinaryStore(string commandParameterIdentifier)
        {
            return _innerExecutionManager.CreateBinaryStore(commandParameterIdentifier);
        }

        public IClientCallInfo CreateCallOptions(string commandIdentifier)
        {
            var clientCallInfo = _innerExecutionManager.CreateCallOptions(commandIdentifier);
            return new MetadataClientCallInfo(clientCallInfo, _serverUuid);
        }
    }
}
