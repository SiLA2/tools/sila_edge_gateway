﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;

namespace Hsrm.EdgeGateway.Services
{
    public class PoolServerResolver : IServerResolver
    {
        private readonly IServerPool _serverPool;
        private readonly IExecutionManagerFactory _executionManagerFactory;

        public PoolServerResolver(IServerPool serverPool, IExecutionManagerFactory executionManagerFactory)
        {
            _serverPool = serverPool;
            _executionManagerFactory = executionManagerFactory;
        }

        public ConnectedServer? ResolveServer(string serverUuid)
        {
            if (Guid.TryParse(serverUuid, out var serverId))
            {
                var connectedServers = _serverPool.ConnectedServers ?? Enumerable.Empty<ServerData>();
                var server = connectedServers.FirstOrDefault(server => server.Config.Uuid == serverId);

                if (server != null)
                {
                    return new ConnectedServer(server, _executionManagerFactory.CreateExecutionManager(server));
                }

                var connectedGateways = connectedServers.Where(server => server.Features.Any(feature => feature.FullyQualifiedIdentifier == "org.silastandard/gateway/GatewayService/v1"));
                var gateway = connectedGateways.FirstOrDefault(gateway => new GatewayServiceClient(gateway.Channel, new DiscoveryExecutionManager()).ConnectedServers.Any(connectedServer => connectedServer.ServerUuid == serverId.ToString()));

                if (gateway != null)
                {
                    var executionManager = _executionManagerFactory.CreateExecutionManager(gateway);
                    return new ConnectedServer(gateway, new MetadataExecutionManager(executionManager, serverUuid));
                }
            }

            return null;
        }
    }
}
