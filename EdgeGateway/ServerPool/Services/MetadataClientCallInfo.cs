﻿using Tecan.Sila2;
using Tecan.Sila2.Client;

namespace Hsrm.EdgeGateway.Services
{
    public class MetadataClientCallInfo : IClientCallInfo
    {
        private readonly IClientCallInfo _innerClientCallInfo;

        public MetadataClientCallInfo(IClientCallInfo innerClientCallInfo, string serverUuid)
        {
            _innerClientCallInfo = innerClientCallInfo;

            Metadata = _innerClientCallInfo.Metadata ?? new Dictionary<string, byte[]>();
            Metadata["org.silastandard/gateway/GatewayService/v1/Metadata/ServerUuid"] = ByteSerializer.ToByteArray(new PropertyResponse<StringDto>(serverUuid));
        }

        public IDictionary<string, byte[]> Metadata { get; }

        public void FinishSuccessful()
        {
            _innerClientCallInfo.FinishSuccessful();
        }

        public void FinishWithErrors(Exception exception)
        {
            _innerClientCallInfo.FinishWithErrors(exception);
        }
    }
}
