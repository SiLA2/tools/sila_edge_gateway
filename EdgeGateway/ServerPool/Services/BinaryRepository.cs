﻿using Hsrm.EdgeGateway.Contracts;
using System.IO;
using Tecan.Sila2;
using Tecan.Sila2.Binary;

namespace Hsrm.EdgeGateway.Serialization
{
    public class BinaryRepository : IBinaryStore, IDisposable
    {
        private readonly Dictionary<string, BinaryInfo> _binaries = [];
        private readonly Timer _timer;

        private const int CleanupIntervalInMs = 5*60_000;
        private bool _isTimerRunning = false;

        public BinaryRepository()
        {
            _timer = new Timer(RemoveExpiredStreams);
        }

        private void RemoveExpiredStreams(object? state)
        {
            var threshold = DateTime.Now;
            lock(_binaries)
            {
                foreach(var bin in _binaries.Values.ToArray())
                {
                    if (bin.Expiration <  threshold)
                    {
                        _binaries.Remove(bin.Identifier);
                        bin.Binary.Dispose();
                    }
                }
                if (_binaries.Count == 0)
                {
                    _timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
                    _isTimerRunning = false;
                }
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public Stream? ResolveStoredBinary(string identifier)
        {
            lock (_binaries)
            {
                if (_binaries.TryGetValue(identifier, out BinaryInfo? binaryInfo))
                {
                    return binaryInfo?.Binary;
                }
            }
            return null;
        }

        public FileInfo ResolveStoredBinaryFile(string identifier)
        {
            throw new NotImplementedException();
        }

        public bool ShouldStoreBinary(long length)
        {
            return length < 500_000;
        }

        public string StoreBinary(Stream data)
        {
            string identifier;
            if (data is LazyBinaryStream binaryStream)
            {
                identifier = binaryStream.BinaryStorageIdentifier;
            }
            else
            {
                identifier = Guid.NewGuid().ToString();
            }
            lock (_binaries)
            {
                _binaries.Add(identifier, new BinaryInfo(data, identifier, DateTime.Now.AddMinutes(10)));
                if (!_isTimerRunning)
                {
                    _timer.Change(CleanupIntervalInMs, CleanupIntervalInMs);
                    _isTimerRunning = true;
                }
            }
            return identifier;
        }

        public string StoreBinary(FileInfo file)
        {
            throw new NotImplementedException();
        }

        public void Delete(string identifier)
        {
        }

        private record BinaryInfo(Stream Binary, string Identifier, DateTime Expiration);
    }
}
