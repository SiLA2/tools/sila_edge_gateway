﻿using Common.Logging;
using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.ServiceDefinition;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.GatewayService
{
    [Export(typeof(IGatewayService))]
    [Export(typeof(IGatewayConfigurationService))]
    [Export(typeof(IGatewayInitialization))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class GatewayService : IGatewayService, INotifyPropertyChanged, IGatewayConfigurationService, IGatewayInitialization
    {
        public IRequestInterceptor ServerUuid => _interceptor;

        public IReadOnlyList<Server> ConnectedServers
        {
            get;
            private set;
        }

        public bool HasNewFeature {
            get;
            private set;
        }

        private static readonly string _featuresDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "SiLA2", "EdgeGateway", "Features");
        private static readonly string _metadataIdentifier = "org.silastandard/gateway/GatewayService/v1/Metadata/ServerUuid";
        private readonly ConcurrentDictionary<string, DynamicFeatureProvider> _features = new ConcurrentDictionary<string, DynamicFeatureProvider>();
        private readonly ILog _logger = LogManager.GetLogger<GatewayService>();
        private readonly GatewayInterceptor _interceptor;
        private readonly IServerDiscovery _serverDiscovery;
        private readonly IServerConnector _connector;
        private readonly ISiLAServer _server;
        private readonly ISiLAService _silaService;
        private readonly IConfigurationStore _configurationStore;
        private readonly ServerStartInformation _serverStartInformation;
        private readonly IServerResolver _serverResolver;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly IExecutionManagerFactory _executionManagerFactory;

        [ImportingConstructor]
        public GatewayService(
            GatewayInterceptor interceptor,
            IServerDiscovery serverDiscovery,
            IServerConnector connector,
            ISiLAServer server,
            ISiLAService silaService,
            IConfigurationStore configurationStore,
            ServerStartInformation serverStartInformation,
            IServerResolver serverResolver,
            IGatewayMonitor gatewayMonitor,
            IExecutionManagerFactory executionManagerFactory)
        {
            _interceptor = interceptor;
            _serverDiscovery = serverDiscovery;
            _connector = connector;
            _server = server;
            _silaService = silaService;
            _configurationStore = configurationStore;
            _serverStartInformation = serverStartInformation;
            _serverResolver = serverResolver;
            _gatewayMonitor = gatewayMonitor;
            _executionManagerFactory = executionManagerFactory;
        }

        public void Initialize()
        {
            if (!Directory.Exists(_featuresDir))
            {
                Directory.CreateDirectory(_featuresDir);
            }

            foreach (var featureFile in Directory.EnumerateFiles(_featuresDir, "*.sila.xml"))
            {
                var feature = FeatureSerializer.Load(featureFile);
                AddFeature(feature);
            }

            HasNewFeature = false;

            Task.Run(() => ScanNetwork(5));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void ScanNetwork([Unit("s", Second = 1)] int duration)
        {
            _serverDiscovery.DiscoverServers(AddServerCore, TimeSpan.FromSeconds(duration), nic => true);
        }

        private void AddFeature(Feature feature)
        {
            var id = feature.FullyQualifiedIdentifier;
            var provider = new DynamicFeatureProvider(feature, _interceptor, _server, _gatewayMonitor);
            if (_features.TryAdd(id, provider) && !_silaService.ImplementedFeatures.Contains(id, StringComparer.OrdinalIgnoreCase))
            {
                _server.AddFeature(provider);
                WriteFeatureFile(feature);
                WriteMetadataConfiguration(feature);

                HasNewFeature = true;
            }
        }

        private void WriteFeatureFile(Feature feature)
        {
            var featurePath = Path.Combine(_featuresDir, feature.FullyQualifiedIdentifier.Replace('/', '.') + ".sila.xml");
            try
            {
                FeatureSerializer.Save(feature, featurePath);
            }
            catch (Exception ex)
            {
                _logger.Error($"Error writing feature file {featurePath}", ex);
            }
        }

        private void WriteMetadataConfiguration(Feature feature)
        {
            var metadataConfiguration = _configurationStore.Read<MetadataConfigurationSet>(_serverStartInformation.ConfigName) ?? new MetadataConfigurationSet();

            var config = metadataConfiguration.Configurations.FirstOrDefault(c => c.Identifier == _metadataIdentifier);

            if (config == null)
            {
                config = new MetadataConfiguration
                {
                    Identifier = _metadataIdentifier,
                    Affected = Array.Empty<string>()
                };
                metadataConfiguration.Configurations.Add(config);
            }

            if (!config.Affected.Contains(feature.FullyQualifiedIdentifier))
            {
                config.Affected = config.Affected.Append(feature.FullyQualifiedIdentifier).ToArray();
            }

            _configurationStore.Write(_serverStartInformation.ConfigName, metadataConfiguration);
        }

        public void AddServer(string host, int port)
        {
            var server = _connector.Connect(host, port);

            AddServerCore(server);
        }

        private void AddServerCore(ServerData server)
        {
            if (server.Config.Uuid.ToString() != _silaService.ServerUUID)
            {
                _interceptor.AddServer(server);

                foreach (var feature in server.Features)
                {
                    AddFeature(feature);
                }

                ConnectedServers = _interceptor.ConnectedServers.Select(s => s.Info).ToList();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ConnectedServers)));
            }
        }

        public void RemoveServer(string serverUuid)
        {
            if (!Guid.TryParse(serverUuid, out var serverGuid) || !_interceptor.RemoveServer(serverGuid))
            {
                throw new ArgumentOutOfRangeException(nameof(serverUuid), $"No server with Id {serverUuid} is connected");
            }
        }

        public void Restart()
        {
            Task.Run(() =>
            {
                var process = new Process
                {
                    StartInfo =
                    {
                        FileName = Environment.ProcessPath
                    }
                };
                process.Start();
                Environment.Exit(0);
            });
        }

        public void SetServerName(string serverUuid, string name)
        {
            var server = _serverResolver.ResolveServer(serverUuid);
            if (server == null) return;

            var executionManager = new DiscoveryExecutionManager();
            var commandClient = new SiLAServiceClient(server.Server.Channel, executionManager);
            commandClient.SetServerName(name);
        }

        public IReadOnlyList<string> GetRequiredMetadata(string serverUuid, string identifier)
        {
            var server = _serverResolver.ResolveServer(serverUuid);
            if (server == null) return new List<string>();

            var executionManager = (IClientExecutionManagerEx)_executionManagerFactory.CreateExecutionManager(server.Server);
            return executionManager.GetRequiredMetadata(identifier).ToList();
        }
    }
}
