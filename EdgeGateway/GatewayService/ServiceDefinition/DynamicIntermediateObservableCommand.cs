﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicIntermediateObservableCommand : DynamicServiceDefinition
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureCommand _command;
        private readonly IGatewayMonitor _gatewayMonitor;

        public DynamicIntermediateObservableCommand(Feature featureDefinition, FeatureCommand command, IServerContextProvider contextProvider, IGatewayMonitor gatewayMonitor)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _command = command;
            _gatewayMonitor = gatewayMonitor;
        }

        public IIntermediateObservableCommand<DynamicObjectProperty, DynamicObjectProperty> Invoke(DynamicRequest request)
        {
            var context = _contextProvider.Context;
            var commandClient = new IntermediateObservableCommandClient(_command, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));
            var commandId = _featureDefinition.GetFullyQualifiedIdentifier(_command);

            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.CommandInvocation, context.ConnectedServer.Info.ServerUuid, commandId));

            var command = commandClient.Invoke(request, context.ConnectedServer.CreateAdditionalMetadata(context.Metadata));
            return new IntermediateObservableMonitoringCommand(command, _gatewayMonitor, context.ConnectedServer.Info.ServerUuid, commandId);
        }
    }
}
