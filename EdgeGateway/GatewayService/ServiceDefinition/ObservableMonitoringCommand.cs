﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class ObservableMonitoringCommand : IObservableCommand<DynamicObjectProperty>
    {
        private readonly IObservableCommand<DynamicObjectProperty> _innerCommand;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly string _serverUuid;
        private readonly string _commandId;

        public ObservableMonitoringCommand(IObservableCommand<DynamicObjectProperty> innerCommand, IGatewayMonitor gatewayMonitor, string serverUuid, string commandId)
        {
            _innerCommand = innerCommand;
            _gatewayMonitor = gatewayMonitor;
            _serverUuid = serverUuid;
            _commandId = commandId;
        }

        public Task<DynamicObjectProperty> Response => _innerCommand.Response.ContinueWith(task =>
        {
            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.CommandResponse, _serverUuid, _commandId));
            return task.Result;
        });

        public StateUpdate State => _innerCommand.State;

        public ChannelReader<StateUpdate> StateUpdates => new MonitoringChannelReader<StateUpdate>(_innerCommand.StateUpdates, _gatewayMonitor, (StateUpdate stateUpdate) => new GatewayMonitoringMessage(_serverUuid, _commandId, stateUpdate));

        public bool IsStarted => _innerCommand.IsStarted;

        public CancellationToken CancellationToken => _innerCommand.CancellationToken;

        public bool IsCancellationSupported => _innerCommand.IsCancellationSupported;

        Task IObservableCommand.Response => Response;

        public void Cancel()
        {
            _innerCommand.Cancel();
        }

        public void Start()
        {
            _innerCommand.Start();
        }
    }

    internal class IntermediateObservableMonitoringCommand : ObservableMonitoringCommand, IIntermediateObservableCommand<DynamicObjectProperty, DynamicObjectProperty>
    {
        private readonly IIntermediateObservableCommand<DynamicObjectProperty, DynamicObjectProperty> _innerCommand;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly string _serverUuid;
        private readonly string _commandId;

        public IntermediateObservableMonitoringCommand(IIntermediateObservableCommand<DynamicObjectProperty, DynamicObjectProperty> innerCommand, IGatewayMonitor gatewayMonitor, string serverUuid, string commandId)
            : base(innerCommand, gatewayMonitor, serverUuid, commandId)
        {
            _innerCommand = innerCommand;
            _gatewayMonitor = gatewayMonitor;
            _serverUuid = serverUuid;
            _commandId = commandId;
        }

        public ChannelReader<DynamicObjectProperty> IntermediateValues => new MonitoringChannelReader<DynamicObjectProperty>(_innerCommand.IntermediateValues, _gatewayMonitor, (DynamicObjectProperty _) => new GatewayMonitoringMessage(MessageType.IntermediateValue, _serverUuid, _commandId));
    }
}
