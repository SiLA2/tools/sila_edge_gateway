﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicUnobservableProperty : DynamicServiceDefinition
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureProperty _property;
        private readonly IGatewayMonitor _gatewayMonitor;

        public DynamicUnobservableProperty(Feature featureDefinition, FeatureProperty property, IServerContextProvider contextProvider, IGatewayMonitor gatewayMonitor)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _property = property;
            _gatewayMonitor = gatewayMonitor;
        }

        public DynamicObjectProperty Invoke()
        {
            var context = _contextProvider.Context;
            var client = new PropertyClient(_property, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));

            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.PropertyRead, context.ConnectedServer.Info.ServerUuid, _featureDefinition.GetFullyQualifiedIdentifier(_property)));

            var innerValue = client.RequestValue(context.ConnectedServer.CreateAdditionalMetadata(context.Metadata));
            var propertyType = new DataTypeType
            {
                Item = new StructureType
                {
                    Element = [new SiLAElement
                    {
                        Identifier = _property.Identifier,
                        Description = _property.Description,
                        DisplayName = _property.DisplayName,
                        DataType = _property.DataType
                    }]
                }
            };
            return new DynamicObjectProperty(_property.Identifier, _property.DisplayName, _property.Description, propertyType)
            {
                Value = new DynamicObject
                {
                    Elements =
                    {
                        innerValue
                    }
                }
            };
        }
    }
}
