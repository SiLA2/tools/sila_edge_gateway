﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class MonitoringChannelReader<T> : ChannelReader<T>
    {
        private readonly ChannelReader<T> _innerChannelReader;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly Func<T, GatewayMonitoringMessage> _gatewayMonitoringMessage;

        public MonitoringChannelReader(ChannelReader<T> innerChannelReader, IGatewayMonitor gatewayMonitor, Func<T, GatewayMonitoringMessage> gatewayMonitoringMessage)
        {
            _innerChannelReader = innerChannelReader;
            _gatewayMonitor = gatewayMonitor;
            _gatewayMonitoringMessage = gatewayMonitoringMessage;
        }

        public override bool TryRead([MaybeNullWhen(false)] out T item)
        {
            var result = _innerChannelReader.TryRead(out item);
            if (result)
            {
                _gatewayMonitor.SendMonitoringMessage(_gatewayMonitoringMessage(item));
            }
            return result;
        }

        public override ValueTask<bool> WaitToReadAsync(CancellationToken cancellationToken = default)
        {
            return _innerChannelReader.WaitToReadAsync(cancellationToken);
        }
    }
}
