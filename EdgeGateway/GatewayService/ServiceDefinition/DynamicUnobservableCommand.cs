﻿using Hsrm.EdgeGateway.Contracts;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicUnobservableCommand : DynamicServiceDefinition
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureCommand _command;
        private readonly IGatewayMonitor _gatewayMonitor;

        public DynamicUnobservableCommand(Feature featureDefinition, FeatureCommand command, IServerContextProvider contextProvider, IGatewayMonitor gatewayMonitor)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _command = command;
            _gatewayMonitor = gatewayMonitor;
        }

        public DynamicObjectProperty Invoke(DynamicRequest arg)
        {
            var context = _contextProvider.Context;
            var commandClient = new NonObservableCommandClient(_command, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));

            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.CommandInvocation, context.ConnectedServer.Info.ServerUuid, _featureDefinition.GetFullyQualifiedIdentifier(_command)));

            return commandClient.Invoke(arg, context.ConnectedServer.CreateAdditionalMetadata(context.Metadata));
        }
    }
}
