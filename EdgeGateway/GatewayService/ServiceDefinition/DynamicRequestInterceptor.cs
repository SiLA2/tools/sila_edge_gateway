﻿using Tecan.Sila2;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicRequestInterceptor : IRequestInterceptor
    {
        public DynamicRequestInterceptor(string metadataIdentifier)
        {
            MetadataIdentifier = metadataIdentifier;
        }

        public int Priority => 3;

        public bool AppliesToCommands => true;

        public bool AppliesToProperties => true;

        public string MetadataIdentifier { get; }

        public IRequestInterception Intercept(string commandIdentifier, ISiLAServer server, IMetadataRepository metadata)
        {
            throw new NotSupportedException();
        }

        public bool IsInterceptRequired(Feature feature, string commandIdentifier)
        {
            return true;
        }
    }
}