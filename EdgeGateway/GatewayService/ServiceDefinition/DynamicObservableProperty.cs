﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicObservableProperty : DynamicServiceDefinition, INotifyPropertyChanged
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureProperty _property;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly ConcurrentDictionary<Guid, SubscriptionData> _values = new ConcurrentDictionary<Guid, SubscriptionData>();

        public DynamicObservableProperty(Feature featureDefinition, FeatureProperty property, IServerContextProvider contextProvider, IGatewayMonitor gatewayMonitor)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _property = property;
            _gatewayMonitor = gatewayMonitor;
        }

        public static DynamicObjectProperty Value => null;

        public event PropertyChangedEventHandler PropertyChanged;

        internal ChannelReader<DynamicObjectProperty> CreateSubscription(CancellationToken cancellationToken)
        {
            var context = _contextProvider.Context;
            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.PropertyRead, context.ConnectedServer.Info.ServerUuid, _featureDefinition.GetFullyQualifiedIdentifier(_property)));

            var channel = Channels.CreateIntermediateChannel<DynamicObjectProperty>();
            
            if (!_values.TryGetValue(context.ConnectedServer.Server.Config.Uuid, out var subscription))
            {
                var propertyClient = new PropertyClient(_property, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));

                subscription = new SubscriptionData(_property);
                propertyClient.Subscribe(subscription.SetValue, subscription.Token);
                _values.AddOrUpdate(context.ConnectedServer.Server.Config.Uuid, subscription, (_, sub) =>
                {
                    subscription.CancelSubscription();
                    return sub;
                });
            }
            subscription.AddWriter(channel.Writer, cancellationToken);

            return channel.Reader;

        }


        private sealed class SubscriptionData
        {
            private readonly CancellationTokenSource _cts = new();
            private readonly List<ChannelWriter<DynamicObjectProperty>> _writers = new List<ChannelWriter<DynamicObjectProperty>>();

            public CancellationToken Token => _cts.Token;

            public DynamicObjectProperty Value { get; set; }

            private readonly FeatureProperty property;

            private readonly DataTypeType propertyType;

            public SubscriptionData(FeatureProperty property)
            {
                this.property = property;
                propertyType = new DataTypeType
                {
                    Item = new StructureType
                    {
                        Element = [new SiLAElement
                        {
                            Identifier = property.Identifier,
                            Description = property.Description,
                            DisplayName = property.DisplayName,
                            DataType = property.DataType
                        }]
                    }
                };
            }

            public void SetValue(DynamicObjectProperty value)
            {
                Value = Encapsulate(value);
                lock (_writers)
                {
                    foreach (var writer in _writers)
                    {
                        writer.TryWrite(Value);
                    }
                }
            }
            private DynamicObjectProperty Encapsulate(DynamicObjectProperty innerValue)
            {
                return new DynamicObjectProperty(property.Identifier, property.DisplayName, property.Description, propertyType)
                {
                    Value = new DynamicObject
                    {
                        Elements =
                        {
                            innerValue
                        }
                    }
                };
            }

            public void CancelSubscription() => _cts.Cancel();

            public void AddWriter(ChannelWriter<DynamicObjectProperty> writer, CancellationToken cancellationToken)
            {
                cancellationToken.Register(() => EndWriter(writer));
                lock (_writers)
                {
                    if (Value != null) writer.TryWrite(Value);
                    _writers.Add(writer);
                }
            }

            private void EndWriter(ChannelWriter<DynamicObjectProperty> writer)
            {
                lock (_writers)
                {
                    _writers.Remove(writer);
                }
                writer.Complete();
            }
        }
    }
}
