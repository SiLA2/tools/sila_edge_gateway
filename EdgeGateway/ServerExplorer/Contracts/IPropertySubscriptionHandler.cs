﻿using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IPropertySubscriptionHandler
    {
        Task<DynamicObjectProperty?> Subscribe(PropertySubscription subscription, IDictionary<string, byte[]>? additionalMetadata = null);

        IEnumerable<PropertySubscription> GetSubscriptions(string serverUuid, string featureId);

        bool CancelSubscription(PropertySubscription subscription);
    }
}
