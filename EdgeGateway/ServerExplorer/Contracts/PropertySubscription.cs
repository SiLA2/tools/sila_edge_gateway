﻿namespace Hsrm.EdgeGateway.Contracts
{
    public record PropertySubscription(string ServerUuid, string FeatureId, string PropertyId);
}
