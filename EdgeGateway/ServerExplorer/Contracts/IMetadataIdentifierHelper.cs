﻿using Hsrm.EdgeGateway.GatewayService;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IMetadataIdentifierHelper
    {
        IReadOnlyList<string> GetRequiredMetadata(string serverUuid, string identifier);
    }
}
