﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Services;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tecan.Sila2;
using Tecan.Sila2.Binary;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Serialization
{
    public class DynamicObjectPropertyConverter : JsonConverter<DynamicObjectProperty>
    {
        public static readonly DynamicObjectPropertyConverter Instance = new DynamicObjectPropertyConverter();

        public IBinaryStore? BinaryStore { get; set; }

        public override DynamicObjectProperty? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            SiLAElement element = ReadElement(ref reader);
            var result = new DynamicObjectProperty(element);
            ReadValue(ref reader, result);
            reader.Read();
            Assert(reader, JsonTokenType.EndObject);
            return result;
        }

        private SiLAElement ReadElement(ref Utf8JsonReader reader)
        {
            Assert(reader, JsonTokenType.StartObject);
            reader.Read();
            var identifier = ReadStringProperty(ref reader, "identifier");
            var displayName = ReadStringProperty(ref reader, "displayName");
            var description = ReadStringProperty(ref reader, "description");
            var dataType = ReadType(ref reader);
            var element = new SiLAElement
            {
                Identifier = identifier,
                DisplayName = displayName,
                Description = description,
                DataType = dataType
            };
            return element;
        }

        private void ReadValue(ref Utf8JsonReader reader, DynamicObjectProperty property)
        {
            Assert(reader, JsonTokenType.PropertyName);
            var actualPropertyName = reader.GetString();
            if (actualPropertyName == "value")
            {
                var element = JsonDocument.ParseValue(ref reader).RootElement;
                DynamicObjectPropertyHelper.FillProperty(property, element, null, BinaryStore);
            }
        }

        private DataTypeType? ReadType(ref Utf8JsonReader reader)
        {
            Assert(reader, JsonTokenType.PropertyName);
            var actualPropertyName = reader.GetString();
            if (actualPropertyName != "type" && actualPropertyName != "dataType")
            {
                return null;
            }
            reader.Read();

            Assert(reader, JsonTokenType.StartObject);
            reader.Read();
            Assert(reader, JsonTokenType.PropertyName);
            actualPropertyName = reader.GetString();
            if (actualPropertyName != "item") {
                return null;
            }
            reader.Read();

            var result = new DataTypeType { Item = ReadDataType(ref reader) };
            Assert(reader, JsonTokenType.EndObject);
            reader.Read();
            return result;
        }

        private object? ReadDataType(ref Utf8JsonReader reader)
        {
            var result = reader.TokenType switch
            {
                JsonTokenType.Number => (BasicType)reader.GetInt32(),
                JsonTokenType.String => reader.GetString()!,
                JsonTokenType.StartObject => ReadListOrStructure(ref reader),
                _ => throw new InvalidOperationException("type definition is invalid")
            };

            reader.Read();
            return result;
        }

        private object? ReadListOrStructure(ref Utf8JsonReader reader)
        {
            reader.Read();
            Assert(reader, JsonTokenType.PropertyName);
            var actualPropertyName = reader.GetString();
            reader.Read();
            switch (actualPropertyName)
            {
                case "dataType":
                    Assert(reader, JsonTokenType.StartObject);
                    reader.Read();
                    Assert(reader, JsonTokenType.PropertyName);
                    actualPropertyName = reader.GetString();
                    if (actualPropertyName != "item")
                    {
                        return null;
                    }
                    reader.Read();
                    var item = ReadDataType(ref reader);
                    Assert(reader, JsonTokenType.EndObject);
                    reader.Read();
                    return new ListType { DataType = new DataTypeType { Item = item } };
                case "element":
                    Assert(reader, JsonTokenType.StartArray);
                    reader.Read();
                    var elements = new List<SiLAElement>();
                    while (reader.TokenType != JsonTokenType.EndArray)
                    {
                        elements.Add(ReadElement(ref reader));
                        reader.Read();
                    }
                    reader.Read();
                    return new StructureType
                    {
                        Element = elements.ToArray()
                    };
                default:
                    throw new JsonException("invalid format");
            }
        }

        private string? ReadStringProperty(ref Utf8JsonReader reader, string propertyName)
        {
            Assert(reader, JsonTokenType.PropertyName);
            var actualPropertyName = reader.GetString();
            if (propertyName != actualPropertyName)
            {
                return null;
            }
            reader.Read();
            Assert(reader, JsonTokenType.String);
            var result = reader.GetString();
            reader.Read();
            return result;
        }

        private void Assert(Utf8JsonReader reader, JsonTokenType tokenType)
        {
            if (reader.TokenType != tokenType)
            {
                throw new JsonException("invalid format");
            }
        }

        public override void Write(Utf8JsonWriter writer, DynamicObjectProperty value, JsonSerializerOptions options)
        {
            WriteCore(writer, value, options, false);
        }

        private void WriteCore(Utf8JsonWriter writer, DynamicObjectProperty value, JsonSerializerOptions options, bool serializeType)
        {
            writer.WriteStartObject();
            writer.WriteString("identifier", value.Identifier);
            // do not serialize displayName and description
            if (serializeType)
            {
                writer.WritePropertyName("type");
                JsonSerializer.Serialize(writer, value.Type, options);
            }
            writer.WritePropertyName("value");
            if (value.Value != null)
            {
                if (value.Value is Stream stream)
                {
                    if (stream is LazyBinaryStream binaryStream)
                    {
                        var myIdentifier = BinaryStore?.StoreBinary(binaryStream);
                        JsonSerializer.Serialize(writer, new LargeBinary
                        {
                            Identifier = myIdentifier!
                        }, options);
                    }
                    else
                    {
                        JsonSerializer.Serialize(writer, new SmallBinary
                        {
                            Value = ReadBytes(stream)
                        }, options);
                    }
                }
                else if (value.Value is DynamicObjectProperty anyTypeValue)
                {
                    WriteCore(writer, anyTypeValue, options, true);
                }
                else
                {
                    JsonSerializer.Serialize(writer, value.Value, value.Value.GetType(), options);
                }
            }
            else
            {
                writer.WriteNullValue();
            }
            writer.WriteEndObject();
        }

        private byte[] ReadBytes(Stream stream)
        {
            if (stream is MemoryStream ms)
            {
                return ms.ToArray();
            }
            else
            {
                ms = new MemoryStream();
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}
