﻿namespace Hsrm.EdgeGateway.Serialization
{
    internal class SmallBinary
    {
        public string Kind => "small";

        public byte[] Value { get; set; }

        public Stream ToStream() { return new MemoryStream(Value); }
    }
}
