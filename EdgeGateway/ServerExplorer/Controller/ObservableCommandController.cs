﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2;
using Hsrm.EdgeGateway.Services;
using Tecan.Sila2.Server.Binary;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/observablecommands/{commandId}")]
    public class ObservableCommandController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;
        private readonly ICommandRegistry _commandRegistry;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly IBinaryStore _binaryStore;

        public ObservableCommandController(IFeatureResolver featureResolver, IServerResolver serverResolver, ICommandRegistry commandRegistry, IGatewayMonitor gatewayMonitor, IBinaryStore binaryStore)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
            _commandRegistry = commandRegistry;
            _gatewayMonitor = gatewayMonitor;
            _binaryStore = binaryStore;
        }

        [HttpPost]
        public ActionResult<ObservableCommandExecution> Invoke(string serverId, string featureId, string commandId, [FromBody] CommandRequestBody requestBody)
        {
            var server = _serverResolver.ResolveServer(serverId);

            if (server == null)
            {
                return BadRequest("Server not found");
            }

            var feature = _featureResolver.Resolve(featureId);

            if (feature == null)
            {
                return BadRequest("Feature not found");
            }

            var command = feature.Items.OfType<FeatureCommand>()
                .FirstOrDefault(c => string.Equals(c.Identifier, commandId, StringComparison.OrdinalIgnoreCase));

            if (command == null)
            {
                return BadRequest("Command not found");
            }
            if (command.Observable == FeatureCommandObservable.No)
            {
                return BadRequest("Command is not observable");
            }
            ObservableCommandClient commandClient = CreateCommandClient(server, feature, command);
            DynamicRequest request;
            IDictionary<string, byte[]>? additionalMetadata;
            try
            {
                (request, additionalMetadata) = CreateRequest(requestBody, server, feature, commandClient);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            try
            {
                SendMonitoringMessage(server, feature, command);
                DynamicCommand response = InvokeCommand(command, commandClient, request, additionalMetadata);
                _commandRegistry.RegisterCommand(response);
                return new ObservableCommandExecution(response.CommandId.CommandId, null);
            }
            catch (DefinedErrorException definedError)
            {
                return new ObservableCommandExecution(null, definedError.Message);
            }
            catch (Exception silaException)
            {
                return new ObservableCommandExecution(null, silaException.Message);
            }
        }

        private static DynamicCommand InvokeCommand(FeatureCommand command, ObservableCommandClient commandClient, DynamicRequest request, IDictionary<string, byte[]>? additionalMetadata)
        {
            if (command.IntermediateResponse != null && command.IntermediateResponse.Length > 0)
            {
                return (DynamicCommand)((IntermediateObservableCommandClient)commandClient).Invoke(request, additionalMetadata);
            }
            return commandClient.Invoke(request, additionalMetadata);
        }

        private static ObservableCommandClient CreateCommandClient(GatewayService.ConnectedServer server, Feature feature, FeatureCommand command)
        {
            var context = new FeatureContext(feature, server.Server, server.ExecutionManager);
            if (command.IntermediateResponse != null && command.IntermediateResponse.Length > 0)
            {
                return new IntermediateObservableCommandClient(command, context);
            }
            return new ObservableCommandClient(command, context);
        }

        private void SendMonitoringMessage(GatewayService.ConnectedServer server, Feature feature, FeatureCommand? command)
        {
            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.CommandInvocation, server.Info.ServerUuid, feature.GetFullyQualifiedIdentifier(command)));
        }

        private (DynamicRequest, IDictionary<string, byte[]>?) CreateRequest(CommandRequestBody requestBody, GatewayService.ConnectedServer server, Feature feature, ObservableCommandClient commandClient)
        {
            var request = commandClient.CreateRequest();
            DynamicObjectPropertyHelper.FillProperty(request, requestBody.parameters, feature, _binaryStore);
            var additionalMetadata = MetadataHelper.GetMetadata(server.Server.Features, requestBody.metadata);
            return (request, additionalMetadata);
        }
    }
}
