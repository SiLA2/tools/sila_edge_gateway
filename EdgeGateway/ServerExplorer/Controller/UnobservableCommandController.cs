﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2.Server.Binary;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/commands/{commandId}")]
    public class UnobservableCommandController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;
        private readonly IGatewayMonitor _gatewayMonitor;
        private readonly IBinaryStore _binaryStore;

        public UnobservableCommandController(IFeatureResolver featureResolver, IServerResolver serverResolver, IGatewayMonitor gatewayMonitor, IBinaryStore binaryStore)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
            _gatewayMonitor = gatewayMonitor;
            _binaryStore = binaryStore;
        }

        [HttpPost]
        public async Task<ActionResult<CommandResult>> Invoke(string serverId, string featureId, string commandId, [FromBody] CommandRequestBody requestBody)
        {
            var server = _serverResolver.ResolveServer(serverId);

            if (server == null)
            {
                return BadRequest("Server not found");
            }

            var feature = _featureResolver.Resolve(featureId);

            if (feature == null)
            {
                return BadRequest("Feature not found");
            }

            var command = feature.Items.OfType<FeatureCommand>()
                .FirstOrDefault(c => string.Equals(c.Identifier, commandId, StringComparison.OrdinalIgnoreCase));

            if (command == null)
            {
                return BadRequest("Command not found");
            }
            if (command.Observable == FeatureCommandObservable.Yes)
            {
                return BadRequest("Command is observable");
            }

            var commandClient = new NonObservableCommandClient(command, new FeatureContext(feature, server.Server, server.ExecutionManager));
            var request = commandClient.CreateRequest();
            Dictionary<string, byte[]>? additionalMetadata = null;
            try
            {
                DynamicObjectPropertyHelper.FillProperty(request, requestBody.parameters, feature, _binaryStore);
                additionalMetadata = MetadataHelper.GetMetadata(server.Server.Features, requestBody.metadata);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            try
            {
                _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.CommandInvocation, server.Info.ServerUuid, feature.GetFullyQualifiedIdentifier(command)));
                var response = await commandClient.InvokeAsync(request, additionalMetadata);
                return new CommandResult(response, null, null);
            }
            catch (DefinedErrorException definedError)
            {
                return new CommandResult(null, new DefinedError(definedError.ErrorIdentifier, definedError.Message), null);
            }
            catch (Exception silaException)
            {
                return new CommandResult(null, null, new UndefinedError(silaException.GetType().Name, silaException.Message));
            }
        }
    }

    public record CommandRequestBody(List<MetadataEntry> metadata, JsonElement parameters);
}
