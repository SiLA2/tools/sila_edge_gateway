﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Services;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.Design;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/properties/{propertyId}")]
    public class PropertyController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;
        private readonly IGatewayMonitor _gatewayMonitor;

        public PropertyController(IFeatureResolver featureResolver, IServerResolver serverResolver, IGatewayMonitor gatewayMonitor)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
            _gatewayMonitor = gatewayMonitor;
        }

        [HttpPost]
        public ActionResult<DynamicObjectProperty> Get(string serverId, string featureId, string propertyId, [FromBody] PropertyRequestBody requestBody)
        {

            var server = _serverResolver.ResolveServer(serverId);

            if (server == null)
            {
                return BadRequest("Server not found");
            }

            var feature = _featureResolver.Resolve(featureId);

            if (feature == null)
            {
                return BadRequest("Feature not found");
            }

            var property = feature.Items.OfType<FeatureProperty>()
                .FirstOrDefault(p => string.Equals(p.Identifier, propertyId, StringComparison.OrdinalIgnoreCase));

            if (property == null)
            {
                return BadRequest("Property not found");
            }

            var propertyClient = new PropertyClient(property, new FeatureContext(feature, server.Server, server.ExecutionManager));
            Dictionary<string, byte[]>? additionalMetadata;
            try
            {
                additionalMetadata = MetadataHelper.GetMetadata(server.Server.Features, requestBody.metadata);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.PropertyRead, server.Info.ServerUuid, feature.GetFullyQualifiedIdentifier(property)));
            return propertyClient.RequestValue(additionalMetadata);
        }
    }

    public record PropertyRequestBody(List<MetadataEntry> metadata);
}
