﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/metadata/{serverId}/{featureId}")]
    public class MetadataController : Controller
    {
        private readonly IServerResolver _serverResolver;
        private readonly IFeatureResolver _featureResolver;
        private readonly IMetadataIdentifierHelper _metadataIdentifierHelper;

        public MetadataController(IServerResolver serverResolver, IFeatureResolver featureResolver, IMetadataIdentifierHelper metadataIdentifierHelper)
        {
            _serverResolver = serverResolver;
            _featureResolver = featureResolver;
            _metadataIdentifierHelper = metadataIdentifierHelper;
        }

        [Route("commands/{commandId}")]
        [HttpGet]
        public ActionResult<IReadOnlyList<string>> GetCommandMetadata(string serverId, string featureId, string commandId)
        {
            var server = _serverResolver.ResolveServer(serverId);

            if (server == null)
            {
                return BadRequest("Server not found");
            }

            var feature = _featureResolver.Resolve(featureId);

            if (feature == null)
            {
                return BadRequest("Feature not found");
            }

            var command = feature.Items.OfType<FeatureCommand>()
                .FirstOrDefault(c => string.Equals(c.Identifier, commandId, StringComparison.OrdinalIgnoreCase));

            if (command == null)
            {
                return BadRequest("Command not found");
            }

            return new ActionResult<IReadOnlyList<string>>(_metadataIdentifierHelper.GetRequiredMetadata(serverId, feature.GetFullyQualifiedIdentifier(command)));
        }

        [Route("properties/{propertyId}")]
        [HttpGet]
        public ActionResult<IReadOnlyList<string>> GetPropertyMetadata(string serverId, string featureId, string propertyId)
        {
            var server = _serverResolver.ResolveServer(serverId);

            if (server == null)
            {
                return BadRequest("Server not found");
            }

            var feature = _featureResolver.Resolve(featureId);

            if (feature == null)
            {
                return BadRequest("Feature not found");
            }

            var property = feature.Items.OfType<FeatureProperty>()
                .FirstOrDefault(p => string.Equals(p.Identifier, propertyId, StringComparison.OrdinalIgnoreCase));

            if (property == null)
            {
                return BadRequest("Property not found");
            }

            return new ActionResult<IReadOnlyList<string>>(_metadataIdentifierHelper.GetRequiredMetadata(serverId, feature.GetFullyQualifiedIdentifier(property)));
        }
    }
}
