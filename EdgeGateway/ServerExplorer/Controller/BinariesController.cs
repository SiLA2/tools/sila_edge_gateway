﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using Tecan.Sila2;
using Tecan.Sila2.Server.Binary;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/binary")]
    public class BinariesController : Controller
    {
        private readonly IFileManagement _fileManagement;
        private readonly IBinaryStore _binaryStore;

        public BinariesController(IFileManagement fileManagement, IBinaryStore binaryRepository)
        {
            _fileManagement = fileManagement;
            _binaryStore = binaryRepository;
        }

        [HttpGet]
        public IActionResult GetFile(string identifier)
        {
            var resolvedStream = _binaryStore.ResolveStoredBinary(identifier);
            return File(resolvedStream ?? _fileManagement.OpenRead(identifier), "application/octet-stream");
        }

        [HttpPut]
        public ActionResult<string> PutFile(IFormFile file)
        {
            var identifier = _fileManagement.CreateNew(file.Length);
            using (var stream = _fileManagement.OpenWrite(identifier))
            {
                file.CopyTo(stream);
            }
            return identifier;
        }
    }
}
