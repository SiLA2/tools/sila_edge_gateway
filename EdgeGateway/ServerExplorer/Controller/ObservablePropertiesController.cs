﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2;
using Hsrm.EdgeGateway.Services;

namespace Hsrm.EdgeGateway.Controllers
{
    [Route("/api/execution/{serverId}/{featureId}/observableproperties")]
    [ApiController]
    public class ObservablePropertiesController : ControllerBase
    {
        private readonly IServerResolver _serverResolver;
        private readonly IPropertySubscriptionHandler _propertySubscriptionHandler;

        public ObservablePropertiesController(IServerResolver serverResolver, IPropertySubscriptionHandler propertySubscriptionHandler)
        {
            _serverResolver = serverResolver;
            _propertySubscriptionHandler = propertySubscriptionHandler;
        }

        [HttpGet]
        public IEnumerable<PropertySubscription> GetSubscriptions(string serverId, string featureId)
        {
            return _propertySubscriptionHandler.GetSubscriptions(serverId, featureId);
        }

        [HttpPost("{propertyId}")]
        public async Task<ActionResult<DynamicObjectProperty>> Subscribe(string serverId, string featureId, string propertyId, [FromBody] PropertyRequestBody requestBody)
        {
            var subscription = new PropertySubscription(serverId, featureId, propertyId);
            var server = _serverResolver.ResolveServer(serverId);
            var additionalMetadata = MetadataHelper.GetMetadata(server.Server.Features, requestBody.metadata);
            var result = await _propertySubscriptionHandler.Subscribe(subscription, additionalMetadata);
            return result == null ? NotFound() : result;
        }

        [HttpDelete("{propertyId}")]
        public ActionResult CancelSubscription(string serverId, string featureId, string propertyId)
        {
            var subscription = new PropertySubscription(serverId, featureId, propertyId);
            return _propertySubscriptionHandler.CancelSubscription(subscription) ? Ok() : NotFound();
        }
    }
}
