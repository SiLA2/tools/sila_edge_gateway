﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Serialization;
using Microsoft.AspNetCore.Http.Json;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2.Server.Binary;

namespace Hsrm.EdgeGateway.Services
{
    public class DynamicObjectPropertyHelper
    {
        private static readonly JsonSerializerOptions _deserializeOptions = new JsonSerializerOptions
        {
            Converters =
            {
                DynamicObjectPropertyConverter.Instance,
            },
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        };

        public static void FillProperty(DynamicObjectProperty property, JsonElement parameters, Feature? feature, IBinaryStore? binaryRepository)
        {
            FillProperty(o => property.Value = o, parameters, property.Type, feature, binaryRepository);
        }

        private static void FillProperty(Action<object> target, JsonElement parameters, DataTypeType type, Feature? feature, IBinaryStore? binaryStore)
        {
            switch (type.Item)
            {
                case BasicType basicType:
                    FillBasicType(target, parameters, basicType, binaryStore);
                    break;
                case ConstrainedType constrainedType:
                    FillProperty(target, parameters, constrainedType.DataType, feature, binaryStore);
                    break;
                case ListType listType:
                    FillList(target, parameters, listType.DataType, feature, binaryStore);
                    break;
                case StructureType structureType:
                    FillStructure(target, parameters, structureType, feature, binaryStore);
                    break;
                case string dataTypeIdentifier:
                    var structure = feature?.Items.OfType<SiLAElement>()
                        .FirstOrDefault(item => string.Equals(item.Identifier, dataTypeIdentifier, StringComparison.OrdinalIgnoreCase));

                    if (structure == null)
                    {
                        return;
                    }

                    FillProperty(target, parameters, structure.DataType, feature, binaryStore);
                    break;
            }
        }

        private static void FillStructure(Action<object> target, JsonElement parameters, StructureType structureType, Feature? feature, IBinaryStore? binaryStore)
        {
            var obj = new DynamicObject();
            if (structureType.Element != null)
            {
                foreach (var elem in structureType.Element)
                {
                    var property = parameters.GetProperty(elem.Identifier);
                    var propertyValue = new DynamicObjectProperty(elem);
                    FillProperty(o => propertyValue.Value = o, property, elem.DataType, feature, binaryStore);
                    obj.Elements.Add(propertyValue);
                }
            }
            target(obj);
        }

        private static void FillList(Action<object> target, JsonElement parameters, DataTypeType dataType, Feature? feature, IBinaryStore? binaryStore)
        {
            var list = new List<object>();
            if (parameters.ValueKind == JsonValueKind.Array)
            {
                for (int i = 0; i < parameters.GetArrayLength(); i++)
                {
                    FillProperty(list.Add, parameters[i], dataType, feature, binaryStore);
                }
            }
            else
            {
                FillProperty(list.Add, parameters, dataType, feature, binaryStore);
            }
            target(list);
        }

        private static void FillBasicType(Action<object> target, JsonElement parameters, BasicType type, IBinaryStore? binaryStore)
        {
            switch (type)
            {
                case BasicType.String:
                    target(parameters.GetString() ?? "");
                    break;
                case BasicType.Integer:
                    target(parameters.GetInt64());
                    break;
                case BasicType.Real:
                    target(parameters.GetDouble());
                    break;
                case BasicType.Boolean:
                    target(parameters.GetBoolean());
                    break;
                case BasicType.Binary:
                    if (parameters.TryGetBytesFromBase64(out var bytes))
                    {
                        target(new MemoryStream(bytes));
                        break;
                    }
                    var stream = binaryStore?.ResolveStoredBinaryFile(parameters.GetString()!);
                    target(stream ?? throw new InvalidOperationException("stream could not be resolved"));
                    break;
                case BasicType.Date:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Time:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Timestamp:
                    target(parameters.GetDateTimeOffset());
                    break;
                case BasicType.Any:
                    var anyObj = parameters.Deserialize<DynamicObjectProperty>(_deserializeOptions);
                    if (anyObj != null)
                    {
                        target(anyObj);
                        break;
                    }
                    throw new NotSupportedException();
                default:
                    break;
            }
        }
    }
}
