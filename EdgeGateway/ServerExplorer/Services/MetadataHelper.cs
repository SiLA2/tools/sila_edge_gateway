﻿using Hsrm.EdgeGateway.Controllers;
using System.Text.RegularExpressions;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2;
using System.Text.Json;

namespace Hsrm.EdgeGateway.Services
{
    public partial class MetadataHelper
    {
        private static readonly Regex regex = CreateMetadataRegex();

        public static Dictionary<string, byte[]>? GetMetadata(IEnumerable<Feature> features, IReadOnlyList<MetadataEntry> metadata)
        {
            if (metadata.Count == 0) return null;

            var metadataDict = new Dictionary<string, byte[]>();
            foreach (var metadataEntry in metadata)
            {
                var match = regex.Match(metadataEntry.Identifier);
                var featureIdentifier = match.Groups["featureIdentifier"].Value;
                var metadataIdentifier = match.Groups["metadataIdentifier"].Value;

                var metadataFeature = features.FirstOrDefault(feature => feature.FullyQualifiedIdentifier == featureIdentifier);
                var featureMetadata = metadataFeature?.Items.OfType<FeatureMetadata>().FirstOrDefault(metadata => metadata.Identifier == metadataIdentifier);

                if (featureMetadata == null || metadataFeature == null)
                {
                    throw new InvalidOperationException($"Metadata {metadataEntry.Identifier} could not be resolved");
                }

                DynamicObjectProperty property = new DynamicObjectProperty(featureMetadata.Identifier, featureMetadata.DisplayName, featureMetadata.Description, featureMetadata.DataType);
                DynamicObjectPropertyHelper.FillProperty(property, metadataEntry.Content, metadataFeature, null);

                var dataTypeResolver = (string identifier) => metadataFeature.Items.OfType<SiLAElement>().FirstOrDefault(dataTypeDefinition => dataTypeDefinition.Identifier == identifier)?.DataType;
                var serializer = new DynamicObjectSerializer(dataTypeResolver);
                var innerBytes = serializer.Serialize(property, null);
                var bytes = ByteSerializer.ToByteArray(new PropertyResponse<byte[]>(innerBytes));

                metadataDict.Add(metadataEntry.Identifier, bytes);
            }

            return metadataDict;
        }

        [GeneratedRegex("^(?<featureIdentifier>.+)/Metadata/(?<metadataIdentifier>.+)$", RegexOptions.Compiled)]
        private static partial Regex CreateMetadataRegex();
    }

    public record MetadataEntry(string Identifier, JsonElement Content);
}
