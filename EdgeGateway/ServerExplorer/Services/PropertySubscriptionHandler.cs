﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.HubConfig;
using Microsoft.AspNetCore.SignalR;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Services
{
    public class PropertySubscriptionHandler : IPropertySubscriptionHandler
    {
        private readonly Dictionary<PropertySubscription, SubscriptionData> _subscriptions = new Dictionary<PropertySubscription, SubscriptionData>();
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;
        private readonly IHubContext<GatewayHub> _hubContext;
        private readonly IGatewayMonitor _gatewayMonitor;

        public PropertySubscriptionHandler(IFeatureResolver featureResolver, IServerResolver serverResolver, IHubContext<GatewayHub> hubContext, IGatewayMonitor gatewayMonitor)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
            _hubContext = hubContext;
            _gatewayMonitor = gatewayMonitor;
        }

        public bool CancelSubscription(PropertySubscription subscription)
        {
            lock (_subscriptions)
            {
                if (_subscriptions.TryGetValue(subscription, out var subscriptionData))
                {
                    subscriptionData.Cancel();
                    _subscriptions.Remove(subscription);
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<PropertySubscription> GetSubscriptions(string serverUuid, string featureId)
        {
            lock (_subscriptions)
            {
                return _subscriptions.Keys.Where(sub => string.Equals(sub.ServerUuid, serverUuid, StringComparison.OrdinalIgnoreCase)
                                                     && string.Equals(sub.FeatureId, featureId, StringComparison.OrdinalIgnoreCase))
                                          .ToList();
            }
        }

        public async Task<DynamicObjectProperty?> Subscribe(PropertySubscription subscription, IDictionary<string, byte[]>? additionalMetadata = null)
        {
            SubscriptionData? subscriptionData;
            lock (_subscriptions)
            {
                if (_subscriptions.TryGetValue(subscription, out subscriptionData))
                {
                    return subscriptionData.LastReceived;
                }
            }

            var server = _serverResolver.ResolveServer(subscription.ServerUuid);
            var feature = _featureResolver.Resolve(subscription.FeatureId);
            var property = feature?.Items.OfType<FeatureProperty>()
                .FirstOrDefault(p => string.Equals(p.Identifier, subscription.PropertyId, StringComparison.OrdinalIgnoreCase));

            if (property == null)
            {
                return null;
            }

            var propertyClient = new PropertyClient(property, new FeatureContext(feature, server.Server, server.ExecutionManager));
            lock (_subscriptions)
            {
                if (_subscriptions.TryGetValue(subscription, out subscriptionData))
                {
                    return subscriptionData.LastReceived;
                }
                subscriptionData = new SubscriptionData(_hubContext, subscription, _featureResolver, _gatewayMonitor);
                _subscriptions.Add(subscription, subscriptionData);
            }

            _ = propertyClient.Subscribe(subscriptionData.ReceiveValue, subscriptionData.Cancellation.Token, additionalMetadata);
            return await subscriptionData.FirstReceived;
        }

        private class SubscriptionData
        {
            private readonly IHubContext<GatewayHub> _hubContext;
            private readonly IGatewayMonitor _gatewayMonitor;
            private readonly IFeatureResolver _featureResolver;
            private readonly string _propertyIdentifier;

            public PropertySubscription Subscription { get; }

            public SubscriptionData(IHubContext<GatewayHub> hubContext, PropertySubscription subscription, IFeatureResolver featureResolver, IGatewayMonitor gatewayMonitor)
            {
                _hubContext = hubContext;
                Subscription = subscription;
                _gatewayMonitor = gatewayMonitor;
                _featureResolver = featureResolver;

                var feature = _featureResolver.Resolve(Subscription.FeatureId);
                var property = feature.Items.OfType<FeatureProperty>()
                    .FirstOrDefault(p => string.Equals(p.Identifier, Subscription.PropertyId, StringComparison.OrdinalIgnoreCase));

                _propertyIdentifier = feature.GetFullyQualifiedIdentifier(property);
            }

            public CancellationTokenSource Cancellation { get; } = new CancellationTokenSource();

            private readonly TaskCompletionSource<DynamicObjectProperty> _firstValue = new TaskCompletionSource<DynamicObjectProperty>();
            private DynamicObjectProperty? _lastValue;

            public DynamicObjectProperty LastReceived => _lastValue ?? _firstValue.Task.Result;

            public Task<DynamicObjectProperty> FirstReceived => _firstValue.Task;

            internal void ReceiveValue(DynamicObjectProperty value)
            {
                _lastValue = value;
                if (!_firstValue.Task.IsCompleted)
                {
                    _firstValue.TrySetResult(value);
                }
                try
                {
                    _gatewayMonitor.SendMonitoringMessage(new GatewayMonitoringMessage(MessageType.PropertyRead, Subscription.ServerUuid, _propertyIdentifier));
                    _hubContext.Clients.All.SendAsync($"/property/{Subscription.ServerUuid}/{Subscription.FeatureId}/{Subscription.PropertyId}", value, Cancellation.Token);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine($"Exception sending new received value for property {Subscription.PropertyId} on server {Subscription.ServerUuid}", ex);
                }
            }

            public void Cancel()
            {
                Cancellation.Cancel();
            }
        }
    }
}
