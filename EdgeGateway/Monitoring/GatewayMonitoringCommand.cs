﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway
{
    public class GatewayMonitoringCommand : ObservableCommand, IIntermediateObservableCommand<GatewayMonitoringMessage>
    {
        private readonly Regex _serverPattern;
        private readonly Channel<GatewayMonitoringMessage> _channel;
        private readonly Action<GatewayMonitoringCommand> _cancellationTask;

        public GatewayMonitoringCommand(int channelCapacity, string serverPattern, Action<GatewayMonitoringCommand> cancellationTask) : base(new CancellationTokenSource())
        {
            _serverPattern = new Regex(serverPattern, RegexOptions.Compiled);

            var defaultOptions = Channels.DefaultOptions;
            var channelOptions = new BoundedChannelOptions(channelCapacity)
            {
                AllowSynchronousContinuations = defaultOptions.AllowSynchronousContinuations,
                SingleReader = defaultOptions.SingleReader,
                SingleWriter = defaultOptions.SingleWriter
            };

            _channel = Channel.CreateBounded<GatewayMonitoringMessage>(channelOptions);
            _cancellationTask = cancellationTask;
        }

        public async override Task Run()
        {
            var waitForCancellation = new TaskCompletionSource(TaskCreationOptions.RunContinuationsAsynchronously);
            var registration = CancellationToken.Register(() => waitForCancellation.SetResult());
            await using var _ = registration.ConfigureAwait(false);

            try
            {
                await waitForCancellation.Task.WaitAsync(CancellationToken).ConfigureAwait(false);
            }
            catch (TaskCanceledException)
            {
                return;
            }
            finally
            {
                _cancellationTask(this);
            }
        }

        public ChannelReader<GatewayMonitoringMessage> IntermediateValues => _channel.Reader;

        public void SendMessage(GatewayMonitoringMessage message)
        {
            if (_serverPattern.IsMatch(message.ServerUuid))
            {
                if (!_channel.Writer.TryWrite(message))
                {
                    throw new ChannelCapacityReachedException();
                }
            }
        }
    }
}
