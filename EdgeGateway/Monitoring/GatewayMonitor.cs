﻿using Common.Logging;
using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Monitoring
{
    [Export(typeof(IGatewayMonitoringService))]
    [Export(typeof(IGatewayMonitor))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class GatewayMonitor : IGatewayMonitoringService, IGatewayMonitor
    {
        private static readonly SynchronizedCollection<GatewayMonitoringCommand> _commands = new SynchronizedCollection<GatewayMonitoringCommand>();
        private static readonly ILog _logger = LogManager.GetLogger<GatewayMonitor>();
        private static readonly int _channelCapacity = 50;

        public IIntermediateObservableCommand<GatewayMonitoringMessage> ListenToServerMessages(string serverPattern)
        {
            var command = new GatewayMonitoringCommand(_channelCapacity, serverPattern, (GatewayMonitoringCommand command) =>
            {
                _commands.Remove(command);
                _logger.Info($"Monitoring channel removed ({command.GetHashCode()})");
            });

            _commands.Add(command);
            _logger.Info($"Monitoring channel added ({command.GetHashCode()})");

            return command;
        }

        public void SendMonitoringMessage(GatewayMonitoringMessage message)
        {
            var logMessage = $"{message.Type} with identifier {message.Identifier} at server {message.ServerUuid} ({message.Time})";
            if (message.Type == MessageType.StateUpdate)
            {
                logMessage += $"\nProgress={message.StateUpdate.Progress:p}, Estimated time={message.StateUpdate.EstimatedRemainingTime}, State={message.StateUpdate.State}";
            }
            _logger.Info(logMessage);

            List<GatewayMonitoringCommand>? toBeRemoved = null;
            foreach (var command in _commands)
            {
                try
                {
                    command.SendMessage(message);
                }
                catch(ChannelCapacityReachedException)
                {
                    toBeRemoved ??= new List<GatewayMonitoringCommand>();
                    toBeRemoved.Add(command);
                }
            }

            if (toBeRemoved != null)
            {
                foreach (var command in toBeRemoved)
                {
                    _commands.Remove(command);
                    _logger.Info($"Monitoring channel removed due to reaching capacity ({command.GetHashCode()})");
                }
            }
        }
    }
}
