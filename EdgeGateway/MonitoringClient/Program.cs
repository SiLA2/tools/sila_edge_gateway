﻿using Tecan.Sila2;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.DynamicClient;
using MonitoringClient.GatewayMonitoringService;
using Tecan.Sila2.Cancellation.CancelController;
using Tecan.Sila2.Client;

var executionManager = new DiscoveryExecutionManager();
var connector = new ServerConnector(executionManager);
var discovery = new ServerDiscovery(connector);

var servers = discovery.GetServers(TimeSpan.FromSeconds(10));

Console.WriteLine("Actual servers in the network");
foreach (var server in servers)
{
    Console.WriteLine(server.Config.Name);
}

var gateway = servers.Single(s => s.Features.Any(f => f.FullyQualifiedIdentifier == "org.silastandard/gateway/GatewayService/v1"));
var gatewayClient = new GatewayMonitoringServiceClient(gateway.Channel, executionManager);

var commandExecution = gatewayClient.ListenToServerMessages(".*");

var cancellationTokenSource = new CancellationTokenSource();
var cancellationToken = cancellationTokenSource.Token;

Console.CancelKeyPress += delegate(object? sender, ConsoleCancelEventArgs args)
{
    var cancellationClient = new CancelControllerClient(gateway.Channel, executionManager);
    cancellationClient.CancelCommand((commandExecution as IClientCommand).CommandId);

    args.Cancel = true;
    cancellationTokenSource.Cancel();
};

Console.WriteLine("Listening for server messages...");

try
{
    while (await commandExecution.IntermediateValues.WaitToReadAsync(cancellationToken))
    {
        if (commandExecution.IntermediateValues.TryRead(out var intermediateValue))
        {
            var logMessage = $"{intermediateValue.Type} with identifier {intermediateValue.Identifier} at server {intermediateValue.ServerUuid} ({intermediateValue.Time})";
            if (intermediateValue.Type == MessageType.StateUpdate)
            {
                logMessage += $"\nProgress={intermediateValue.StateUpdate.Progress:p}, Estimated time={intermediateValue.StateUpdate.EstimatedRemainingTime}, State={intermediateValue.StateUpdate.State}";
            }
            Console.WriteLine(logMessage);
        }
    }
}
catch (OperationCanceledException)
{
    Console.WriteLine("Monitoring has been stopped");
}

Console.ReadLine();