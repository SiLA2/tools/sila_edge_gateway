﻿using System.Threading.Channels;
using Tecan.Sila2;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.DynamicClient;
using TestClient.GatewayService;
using TestClient.MetadataProvider;

var executionManager = new DiscoveryExecutionManager();
var connector = new ServerConnector(executionManager);
var discovery = new ServerDiscovery(connector);

var servers = discovery.GetServers(TimeSpan.FromSeconds(10));

Console.WriteLine("Actual servers in the network");
foreach (var server in servers)
{
    Console.WriteLine(server.Config.Name);
}

var gateway = servers.Single(s => s.Features.Any(f => f.FullyQualifiedIdentifier == "org.silastandard/gateway/GatewayService/v1"));
var gatewayClient = new GatewayServiceClient(gateway.Channel, executionManager);

void ShowConnectedServers()
{
    var connectedServers = gatewayClient!.ConnectedServers ?? Enumerable.Empty<TestClient.Server>();
    Console.WriteLine($"{connectedServers.Count()} connected servers");

    foreach (var server in connectedServers)
    {
        Console.WriteLine($" - {server.Name} ({server.ServerUuid})");

        if (server.Features.Contains("org.silastandard/test/ObservableCommandTest/v1") && server.ServerUuid != gateway.Config.Uuid.ToString())
        {
            var feature = gateway.Features.Single(f => f.FullyQualifiedIdentifier == "org.silastandard/test/ObservableCommandTest/v1");
            var command = feature.Items.OfType<FeatureCommand>().Single(c => c.Identifier == "Count");
            var commandClient = new IntermediateObservableCommandClient(command, new FeatureContext(feature, gateway, executionManager));
            var request = commandClient.CreateRequest();

            request.Value = new DynamicObject
            {
                Elements =
                {
                    new DynamicObjectProperty(command.Parameter[0]) { Value = (long) 10},
                    new DynamicObjectProperty(command.Parameter[1]) { Value = 1.0},
                }
            };

            try
            {
                var response = commandClient.Invoke(request, new Dictionary<string, byte[]>
                {
                    ["org.silastandard/gateway/GatewayService/v1/Metadata/ServerUuid"] = ByteSerializer.ToByteArray(new PropertyResponse<StringDto>(server.ServerUuid)),
                });

                ReadStateUpdate(response.StateUpdates);
                ReadIntermediateValues(response.IntermediateValues);

                Console.WriteLine(response.Response.Result);
                Console.WriteLine("  " + response.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}

async void ReadStateUpdate(ChannelReader<StateUpdate> stateUpdates)
{
    while (await stateUpdates.WaitToReadAsync())
    {
        if (stateUpdates.TryRead(out var stateUpdate))
        {
            Console.WriteLine($"Progress is {stateUpdate.Progress:p}, Estimated time={stateUpdate.EstimatedRemainingTime}, State={stateUpdate.State}");
        }
    }
}

async void ReadIntermediateValues(ChannelReader<DynamicObjectProperty> intermediateValues)
{
    while (await intermediateValues.WaitToReadAsync())
    {
        if (intermediateValues.TryRead(out var intermediateValue))
        {
            Console.WriteLine($"Intermediate value {intermediateValue.Identifier}: {intermediateValue.Value}");
        }
    }
}

ShowConnectedServers();
gatewayClient.PropertyChanged += (sender, e) => ShowConnectedServers();

Console.ReadLine();