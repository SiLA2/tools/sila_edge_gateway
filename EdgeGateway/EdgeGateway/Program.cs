using Grpc.AspNetCore.Server.Model;
using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Controllers;
using Hsrm.EdgeGateway.HubConfig;
using Hsrm.EdgeGateway.Monitoring;
using Hsrm.EdgeGateway.Serialization;
using Hsrm.EdgeGateway.Services;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Client.ExecutionManagement;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Server;

var serverInfo = new ServerStartInformation(new ServerInformation("EdgeGateway", "A gateway server that makes servers available", "https://www.hs-rm.de", "0.1"), 7297, "*", Guid.Empty, null);
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Host.UseDryIoc(container =>
{
    container.LoadComponentsFromApplicationDirectory();
    container.AddSila2Defaults();
});

var converter = DynamicObjectPropertyConverter.Instance;
builder.Services.AddControllersWithViews()
                .AddApplicationPart(typeof(FeatureController).Assembly)
                .AddJsonOptions(json => json.JsonSerializerOptions.Converters.Add(converter));
builder.Services.AddSila2(serverInfo);

builder.Services.AddSingleton<IServerConnector>(sp => new ServerConnector(new DiscoveryExecutionManager()));
builder.Services.AddSingleton<IServerDiscovery, ServerDiscovery>();
builder.Services.AddSingleton<IFeatureResolver, FeatureResolver>();
builder.Services.AddSingleton<ICommandRegistry, HubCommandRegistry>();
builder.Services.AddSingleton<IPropertySubscriptionHandler, PropertySubscriptionHandler>();
builder.Services.AddSingleton<IExecutionManagerFactory, ExecutionManagerFactory>();
builder.Services.AddSingleton<IServiceConfigurationBuilder<IServerPool, ServiceMethodProviderContext<IServerPool>>, Tecan.Sila2.ServerPooling.ServiceConfigurationBuilder>();
builder.Services.AddSingleton<IGatewayMonitor, GatewayMonitor>();
builder.Services.AddSingleton<IMetadataIdentifierHelper, MetadataIdentifierHelper>();
builder.Services.AddSingleton<IBinaryStore>(sp => sp.GetRequiredService<ISiLAServer>());

builder.Services.AddSignalR();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("ProxyCorsPolicy", builder => builder
        .WithOrigins("https://localhost:44408")
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());
});

var app = builder.Build();

app.Services.InitializeLogging();
converter.BinaryStore = app.Services.GetRequiredService<IBinaryStore>();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
else
{
    app.UseCors("ProxyCorsPolicy");
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapSila2();

app.MapHub<GatewayHub>("/hub");

app.MapFallbackToFile("index.html");

app.Services.GetRequiredService<IGatewayInitialization>().Initialize();

await app.RunAsync();
