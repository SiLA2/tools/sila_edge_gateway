import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { firstValueFrom } from 'rxjs';
import { FeatureDescription, FeatureService, ServerInfo } from 'src/generated-sources/openapi';
import { FeatureMaturityLevel } from '../component-service/interfaces';
import { MatListModule } from '@angular/material/list';

@Component({
  selector: 'app-feature-list',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatRippleModule,
    MatTooltipModule,
    MatListModule
  ],
  templateUrl: './feature-list.component.html',
  styleUrl: './feature-list.component.css'
})
export class FeatureListComponent {
  private _server?: ServerInfo;
  get selectedServer(): ServerInfo | undefined {
    return this._server;
  }
  @Input({required: true}) set selectedServer(server: ServerInfo | undefined) {
    this._server = server;
    this.setSelectedFeature(undefined);
    this.getFeatureDescriptions(server?.features || []);
  }

  features: FeatureDescription[] = [];
  selectedFeature?: FeatureDescription;
  @Output() selectedFeatureChange = new EventEmitter<FeatureDescription>();

  featureMaturityLevel = FeatureMaturityLevel;

  constructor(
    private featureService: FeatureService
  ) {}

  async getFeatureDescriptions(featureIdentifiers: string[]) {
    var result: FeatureDescription[] = [];

    var standardFeatures: FeatureDescription[] = [];
    for (var featureIdentifier of featureIdentifiers) {
      var identifier = featureIdentifier.replace(/\//g, "_");
      var featureDescription = await firstValueFrom(this.featureService.apiFeatureIdentifierGet(identifier));

      if (featureIdentifier.startsWith("org.silastandard/core")) {
        standardFeatures.push(featureDescription);
      } else {
        result.push(featureDescription);
      }
    }

    result.sort((a, b) => a.displayName!.localeCompare(b.displayName!));
    standardFeatures.sort((a, b) => a.displayName!.localeCompare(b.displayName!));

    this.features = result.concat(standardFeatures);
  }

  setSelectedFeature(feature: FeatureDescription | undefined) {
    if (feature == this.selectedFeature) return;
    this.selectedFeature = feature;
    this.selectedFeatureChange.emit(feature);
  }
}
