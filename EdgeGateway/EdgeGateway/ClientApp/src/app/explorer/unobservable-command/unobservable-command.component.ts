import { Component, Inject, ViewChild } from '@angular/core';
import { CommandRequestBody, FeatureCommand, FeatureDescription, ServerInfo, UnobservableCommandService } from 'src/generated-sources/openapi';
import { ParameterAreaComponent } from '../parameter-area/parameter-area.component';
import { MatButtonModule } from '@angular/material/button';
import { MetadataAreaComponent } from '../metadata-area/metadata-area.component';
import { firstValueFrom } from 'rxjs';
import { ResultAreaComponent } from '../result-area/result-area.component';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { DefinedExecutionError, ErrorAreaComponent, UndefinedExecutionError } from '../error-area/error-area.component';

@Component({
  selector: 'app-unobservable-command',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    ParameterAreaComponent,
    MetadataAreaComponent,
    ResultAreaComponent,
    ErrorAreaComponent,
    MatListModule
  ],
  templateUrl: './unobservable-command.component.html',
  styleUrl: './unobservable-command.component.css'
})
export class UnobservableCommandComponent {
  @ViewChild(ParameterAreaComponent) parameterArea!: ParameterAreaComponent;
  @ViewChild(MetadataAreaComponent) metadataArea!: MetadataAreaComponent;
  @ViewChild(ResultAreaComponent) resultAreaComponent!: ResultAreaComponent;
  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;

  resultAreaVisible = false;
  errorAreaVisible = false;

  constructor(
    @Inject("server") public server: ServerInfo,
    @Inject("feature") public feature: FeatureDescription,
    @Inject("command") public command: FeatureCommand,

    private unobservableCommandService: UnobservableCommandService
  ) {}

  async executeUnobservableCommand() {
    try {
      this.hideDisplayAreas();
      await this.executeCommand();

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  private hideDisplayAreas() {
    this.resultAreaVisible = false;
    this.errorAreaVisible = false;
  }

  private async executeCommand() {
    var commandRequestBody: CommandRequestBody = {
      metadata: this.metadataArea.getMetadata(),
      parameters: this.parameterArea.getParameters()
    };

    var featureIdentifier = this.feature.identifier!.replace(/\//g, "_");

    var commandResult = await firstValueFrom(
      this.unobservableCommandService.apiExecutionServerIdFeatureIdCommandsCommandIdPost(this.server.serveruuid!, featureIdentifier, this.command.identifier!, commandRequestBody));
    
    if (commandResult.definedError) throw new DefinedExecutionError(commandResult.definedError);
    if (commandResult.undefinedError) throw new UndefinedExecutionError(commandResult.undefinedError);

    this.resultAreaVisible = true;
    this.resultAreaComponent.setValue(commandResult.result?.value);
  }
}
