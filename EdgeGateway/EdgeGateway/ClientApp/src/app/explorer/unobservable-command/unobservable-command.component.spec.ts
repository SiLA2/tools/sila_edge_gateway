import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnobservableCommandComponent } from './unobservable-command.component';

describe('UnobservableCommandComponent', () => {
  let component: UnobservableCommandComponent;
  let fixture: ComponentFixture<UnobservableCommandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UnobservableCommandComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UnobservableCommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
