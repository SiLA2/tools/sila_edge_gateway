import { CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, Inject, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MetadataAreaComponent } from '../metadata-area/metadata-area.component';
import { DynamicObjectProperty, FeatureDescription, FeatureProperty, ObservablePropertiesService, PropertyRequestBody, PropertyService, ServerInfo } from 'src/generated-sources/openapi';
import { firstValueFrom } from 'rxjs';
import { PropertyValueAreaComponent } from '../property-value-area/property-value-area.component';
import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { ErrorAreaComponent } from '../error-area/error-area.component';

@Component({
  selector: 'app-observable-property',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MetadataAreaComponent,
    PropertyValueAreaComponent,
    ErrorAreaComponent
  ],
  templateUrl: './observable-property.component.html',
  styleUrl: './observable-property.component.css'
})
export class ObservablePropertyComponent implements AfterViewInit {
  @ViewChild(MetadataAreaComponent) metadataArea!: MetadataAreaComponent;
  @ViewChild(PropertyValueAreaComponent) propertyValueAreaComponent!: PropertyValueAreaComponent;
  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;

  featureIdentifier: string;
  hubConnection?: HubConnection;
  propertyDate?: Date;

  propertyAreaVisible = false;
  errorAreaVisible = false;

  connectedToHub = false;
  subscriptionExisted = false;

  constructor(
    @Inject("server") public server: ServerInfo,
    @Inject("feature") public feature: FeatureDescription,
    @Inject("property") public property: FeatureProperty,

    private propertyService: PropertyService,
    private observablePropertiesService: ObservablePropertiesService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.featureIdentifier = this.feature.identifier!.replace(/\//g, "_");
  }

  async ngAfterViewInit() {
    await new Promise(f => setTimeout(f, 100));
    this.getPropertyValue();
  }

  async getPropertyValue() {
    try {
      this.hideDisplayAreas();

      var propertyRequestBody: PropertyRequestBody = {
        metadata: this.metadataArea.getMetadata()
      };
  
      var propertyValue = await firstValueFrom(this.propertyService.apiExecutionServerIdFeatureIdPropertiesPropertyIdPost(this.server.serveruuid!, this.featureIdentifier, this.property.identifier!, propertyRequestBody));
  
      this.setPropertyValue(propertyValue);
      
    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async subscribeToProperty() {
    try {
      this.hideDisplayAreas();

      var subscriptionExists = await this.checkPropertySubscriptions();
      this.subscriptionExisted = subscriptionExists;
      if (!subscriptionExists) {
        await this.createPropertySubscription();
      }

      this.hubConnection = new HubConnectionBuilder()
        .withUrl("/hub", HttpTransportType.ServerSentEvents)
        .configureLogging(LogLevel.Trace)
        .build();

      await this.hubConnection.start();
      this.connectedToHub = true;

      this.hubConnection.on(`/property/${this.server.serveruuid!}/${this.featureIdentifier}/${this.property.identifier}`, (propertyValue: DynamicObjectProperty) => {
        this.setPropertyValue(propertyValue);
      });

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  private hideDisplayAreas() {
    this.propertyAreaVisible = false;
    this.errorAreaVisible = false;
  }

  private async checkPropertySubscriptions(): Promise<boolean> {
    var propertySubscriptions = await firstValueFrom(this.observablePropertiesService.apiExecutionServerIdFeatureIdObservablepropertiesGet(this.server.serveruuid!, this.featureIdentifier));
    return propertySubscriptions.some(subscription => subscription.propertyId == this.property.identifier);
  }

  private async createPropertySubscription() {
    var propertyRequestBody: PropertyRequestBody = {
      metadata: this.metadataArea.getMetadata()
    };

    var propertyValue = await firstValueFrom(this.observablePropertiesService.apiExecutionServerIdFeatureIdObservablepropertiesPropertyIdPost(this.server.serveruuid!, this.featureIdentifier, this.property.identifier!, propertyRequestBody));

    this.setPropertyValue(propertyValue);
  }

  private setPropertyValue(propertyValue: DynamicObjectProperty) {
    this.propertyDate = new Date();
    this.propertyAreaVisible = true;
    this.propertyValueAreaComponent.setValue(propertyValue);
    this.changeDetectorRef.detectChanges();
  }

  async unsubscribeFromProperty() {
    try {
      this.hubConnection?.stop();
      this.connectedToHub = false;
  
      if (!this.subscriptionExisted) {
        await firstValueFrom(this.observablePropertiesService.apiExecutionServerIdFeatureIdObservablepropertiesPropertyIdDelete(this.server.serveruuid!, this.featureIdentifier, this.property.identifier!));
      }

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }
}
