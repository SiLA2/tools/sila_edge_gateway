import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservablePropertyComponent } from './observable-property.component';

describe('ObservablePropertyComponent', () => {
  let component: ObservablePropertyComponent;
  let fixture: ComponentFixture<ObservablePropertyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ObservablePropertyComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ObservablePropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
