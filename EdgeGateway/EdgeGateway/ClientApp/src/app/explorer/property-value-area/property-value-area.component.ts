import { CommonModule } from '@angular/common';
import { Component, ComponentRef, Input, ViewChild, ViewContainerRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { ComponentService, OutputComponent } from '../component-service/component.service';
import { DynamicObjectProperty, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-property-value-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './property-value-area.component.html',
  styleUrl: './property-value-area.component.css'
})
export class PropertyValueAreaComponent {
  @Input({ required: true }) feature!: FeatureDescription;
  @Input({ required: true }) property!: FeatureProperty;

  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<OutputComponent>;

  fullyQualifiedIdentifier!: string;

  constructor(
    private componentService: ComponentService
  ) {}

  ngAfterViewInit() {
    this.fullyQualifiedIdentifier = `${this.feature.identifier}/Property/${this.property.identifier}`;
    this.createOutputComponents();
  }

  createOutputComponents() {
    var componentType = this.componentService.getOutputComponent(this.property.identifier!, this.fullyQualifiedIdentifier, this.property.dataType!);
    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.fullyQualifiedIdentifier, this.property.dataType?.item);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }

  setValue(propertyValue: DynamicObjectProperty) {
    this.componentRef.instance.setValue(propertyValue.value);
  }
}
