import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyValueAreaComponent } from './property-value-area.component';

describe('PropertyValueAreaComponent', () => {
  let component: PropertyValueAreaComponent;
  let fixture: ComponentFixture<PropertyValueAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PropertyValueAreaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PropertyValueAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
