import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandPropertyAreaComponent } from './command-property-area.component';

describe('CommandPropertyAreaComponent', () => {
  let component: CommandPropertyAreaComponent;
  let fixture: ComponentFixture<CommandPropertyAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommandPropertyAreaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CommandPropertyAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
