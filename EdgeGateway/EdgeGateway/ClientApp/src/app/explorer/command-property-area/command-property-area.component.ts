import { Component, ComponentRef, Injector, Input, ViewChild, ViewContainerRef } from '@angular/core';
import { UnobservableCommandComponent } from '../unobservable-command/unobservable-command.component';
import { UnobservablePropertyComponent } from '../unobservable-property/unobservable-property.component';
import { ObservableCommandComponent } from '../observable-command/observable-command.component';
import { ObservablePropertyComponent } from '../observable-property/observable-property.component';
import { FeatureCommand, FeatureCommandObservable, FeatureDescription, FeatureProperty, FeaturePropertyObservable, ServerInfo } from 'src/generated-sources/openapi';
import { MatListModule } from '@angular/material/list';

@Component({
  selector: 'app-command-property-area',
  standalone: true,
  imports: [
    MatListModule
  ],
  templateUrl: './command-property-area.component.html',
  styleUrl: './command-property-area.component.css'
})
export class CommandPropertyAreaComponent {
  @Input({required: true}) selectedServer?: ServerInfo;
  @Input({required: true}) selectedFeature?: FeatureDescription;

  private _selectedItem?: FeatureCommand | FeatureProperty;
  get selectedItem(): FeatureCommand | FeatureProperty | undefined {
    return this._selectedItem;
  }
  @Input({required: true}) set selectedItem(item: FeatureCommand | FeatureProperty | undefined) {
    this._selectedItem = item;
    if (item) this.createComponent(item);
  }

  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef?: ComponentRef<UnobservableCommandComponent | UnobservablePropertyComponent | ObservableCommandComponent | ObservablePropertyComponent>;

  createComponent(item: FeatureCommand | FeatureProperty) {
    var itemType = "parameter" in item ? "command" : "property";

    var injector = Injector.create({
      providers: [
        {
          provide: "server",
          useValue: this.selectedServer
        },
        {
          provide: "feature",
          useValue: this.selectedFeature
        },
        {
          provide: itemType,
          useValue: item
        }
      ]
    });

    var index = this.viewContainerRef.indexOf(this.componentRef?.hostView!);
    if (index != -1) this.viewContainerRef.remove(index);

    if (itemType == "command") {
      this.createCommandComponent(item, injector);
    } else {
      this.createPropertyComponent(item, injector);
    }

    this.componentRef?.changeDetectorRef.detectChanges();
  }

  createCommandComponent(command: FeatureCommand, injector: Injector) {
    if (command.observable == FeatureCommandObservable.NUMBER_0) {
      this.componentRef = this.viewContainerRef.createComponent(ObservableCommandComponent, {injector: injector});
    } else {
      this.componentRef = this.viewContainerRef.createComponent(UnobservableCommandComponent, {injector: injector});
    }
  }

  createPropertyComponent(property: FeatureProperty, injector: Injector) {
    if (property.observable == FeaturePropertyObservable.NUMBER_0) {
      this.componentRef = this.viewContainerRef.createComponent(ObservablePropertyComponent, {injector: injector});
    } else {
      this.componentRef = this.viewContainerRef.createComponent(UnobservablePropertyComponent, {injector: injector});
    }
  }
}
