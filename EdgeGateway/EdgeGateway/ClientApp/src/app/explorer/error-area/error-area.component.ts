import { CommonModule } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { HttpError } from '@microsoft/signalr';
import { DefinedError, FeatureDefinedExecutionError, UndefinedError } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-error-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './error-area.component.html',
  styleUrl: './error-area.component.css'
})
export class ErrorAreaComponent {
  @Input({required: true}) definedExecutionErrors!: FeatureDefinedExecutionError[];
  
  errorType?: ErrorType;
  httpErrorResponse?: HttpErrorResponse;
  definedError?: DefinedError;
  definedExecutionError?: FeatureDefinedExecutionError;
  undefinedError?: UndefinedError;
  errorMessage?: string;

  setError(error: any) {
    if (error instanceof HttpErrorResponse) this.setHttpError(error);
    else if (error instanceof DefinedExecutionError) this.setDefinedError(error)
    else if (error instanceof UndefinedExecutionError) this.setUndefinedError(error);
    else if (error instanceof Error) this.setGenericError(error);
  }

  private setHttpError(httpErrorResponse: HttpErrorResponse) {
    this.errorType = ErrorType.HttpError;
    this.httpErrorResponse = httpErrorResponse;
  }

  private setDefinedError(definedExecutionError: DefinedExecutionError) {
    this.errorType = ErrorType.DefinedError;
    this.definedError = definedExecutionError.definedError;
    this.definedExecutionError = this.definedExecutionErrors.find(error => 
      error.identifier == definedExecutionError.definedError.identifier?.substring(definedExecutionError.definedError.identifier.lastIndexOf("/") + 1));
  }

  private setUndefinedError(undefinedExecutionError: UndefinedExecutionError) {
    this.errorType = ErrorType.UndefinedError;
    this.undefinedError = undefinedExecutionError.undefinedError;
  }

  private setGenericError(error: Error) {
    this.errorType = ErrorType.GenericError;
    this.errorMessage = error.message;
  }
}

enum ErrorType {
  HttpError,
  DefinedError,
  UndefinedError,
  GenericError
}

export class DefinedExecutionError extends Error {
  constructor(public definedError: DefinedError) {
    super(definedError.message || undefined);
  }
}

export class UndefinedExecutionError extends Error {
  constructor(public undefinedError: UndefinedError) {
    super(undefinedError.message || undefined);
  }
}
