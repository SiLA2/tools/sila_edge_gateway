import { Type } from "@angular/core";
import { InputComponent, OutputComponent } from "./component.service";
import { InputExampleComponent } from "./custom/custom-component-example/input-example.component";
import { BinaryOutputAsImageComponent } from "./custom/binary-output-as-image/binary-output-as-image.component";
import { StringInputComponent } from "./default/input/basic-input/string-input/string-input.component";

export class ComponentRegistry {
  private components: RegistryEntry[] = [
    // create new entries for custom components here
    {
      component: InputExampleComponent,
      type: ComponentType.Input,
      predicate: {
        "String": ["org.silastandard/test/UnobservableCommandTest/v1/Command/JoinIntegerAndString"],
        "StringMetadata": ["org.silastandard/test/MetadataProvider/v1"],
        "ConstrainedParameter": ["org.silastandard/test/ParameterConstraintsTest/v1/Command/CheckStringConstraintSet"]
      }
    },
    {
      component: BinaryOutputAsImageComponent,
      type: ComponentType.Output,
      predicate: {
        "ReceivedValue": ["org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue"]
      }
    },
    {
      component: StringInputComponent,
      type: ComponentType.Input,
      predicate: {
        "application/xml": ["org.silastandard/test/ParameterConstraintsTest/v1/Command/CheckBinaryConstraintContentType"]
      }
    }
  ];

  private inputComponents: Map<string, ComponentContainer<InputComponent>> = new Map();
  private outputComponents: Map<string, ComponentContainer<OutputComponent>> = new Map();

  constructor() {
    this.registerComponents();
  }

  private registerComponents() {
    this.components.forEach(registryEntry => {
      if (registryEntry.type == ComponentType.Input) {
        this.registerComponent(this.inputComponents, registryEntry);
      } else {
        this.registerComponent(this.outputComponents, registryEntry);
      }
    });
  }

  private registerComponent(componentContainers: Map<string, ComponentContainer<InputComponent | OutputComponent>>, registryEntry: RegistryEntry) {
    for (let componentIdentifier in registryEntry.predicate) {
      var contextIdentifiers = registryEntry.predicate[componentIdentifier];

      var componentContainer = componentContainers.get(componentIdentifier) || new ComponentContainer();
      componentContainer.registerComponent(registryEntry.component, contextIdentifiers);
      componentContainers.set(componentIdentifier, componentContainer);
    }
  }

  getCustomInputComponent(componentIdentifier: string, fullyQualifiedIdentifier: string): Type<InputComponent> | undefined {
    return this.inputComponents.get(componentIdentifier)?.getComponent(fullyQualifiedIdentifier);
  }

  getCustomOutputComponent(componentIdentifier: string, fullyQualifiedIdentifier: string): Type<OutputComponent> | undefined {
    return this.outputComponents.get(componentIdentifier)?.getComponent(fullyQualifiedIdentifier);
  }
}

export class ComponentContainer<ComponentType extends InputComponent | OutputComponent> {
  private components: Map<string, Type<ComponentType>> = new Map();

  registerComponent(component: Type<ComponentType>, contextIdentifiers: string[]) {
    contextIdentifiers.forEach(contextIdentifier => {
      if (this.components.has(contextIdentifier)) {
        console.log(`Error registering custom component: Duplicate definition of context identifier ${contextIdentifier}`);
      } else {
        this.components.set(contextIdentifier, component);
      }
    });
  }

  getComponent(fullyQualifiedIdentifier: string): Type<ComponentType> | undefined {
    for (var contextIdentifier = fullyQualifiedIdentifier; contextIdentifier.length > 0; contextIdentifier = contextIdentifier.substring(0, contextIdentifier.lastIndexOf("/"))) {
      var component = this.components.get(contextIdentifier);
      if (component) return component;
    }
    return this.components.get("");
  }
}

export interface RegistryEntry {
  component: Type<InputComponent | OutputComponent>;
  type: ComponentType;
  predicate: ComponentPredicate;
}

export enum ComponentType {
  Input,
  Output
}

export interface ComponentPredicate {
  [identifier: string]: string[];
}