import { DataTypeType, SiLAElement } from "src/generated-sources/openapi";

export enum FeatureMaturityLevel {
  Draft,
  Verified,
  Normative
}

export interface ConstrainedType {
    dataType: DataTypeType;
    constraints: Constraints;
}

export interface Constraints {
  length: string,
  minimalLength: string,
  maximalLength: string,
  set: string[],
  pattern: string,
  maximalExclusive: string,
  maximalInclusive: string,
  minimalExclusive: string,
  minimalInclusive: string,
  unit: ConstraintsUnit,
  contentType: ConstraintsContentType,
  elementCount: string,
  minimalElementCount: string,
  maximalElementCount: string,
  fullyQualifiedIdentifier: ConstraintsFullyQualifiedIdentifier,
  fullyQualifiedIdentifierSpecified: boolean,
  schema: ConstraintsSchema,
  allowedTypes: DataTypeType[]
}

export interface ConstraintsUnit {
  label: string,
  factor: number,
  offset: number,
  unitComponent: ConstraintsUnitUnitComponent
}

export interface ConstraintsUnitUnitComponent {
  sIUnit: ConstraintsUnitUnitComponentSIUnit,
  exponent: string
}

export enum ConstraintsUnitUnitComponentSIUnit {
  Dimensionless,
  Meter,
  Kilogram,
  Second,
  Ampere,
  Kelvin,
  Mole,
  Candela
}

export interface ConstraintsContentType {
  type: string,
  subtype: string,
  parameters: ConstraintsContentTypeParameter[]
}

export interface ConstraintsContentTypeParameter {
  attribute: string,
  value: string
}

export enum ConstraintsFullyQualifiedIdentifier {
  FeatureIdentifier,
  CommandIdentifier,
  CommandParameterIdentifier,
  CommandResponseIdentifier,
  IntermediateCommandResponseIdentifier,
  DefinedExecutionErrorIdentifier,
  PropertyIdentifier,
  TypeIdentifier,
  MetadataIdentifier
}

export interface ConstraintsSchema {
  type: ConstraintsSchemaType,
  item: string,
  itemElementName: ItemChoiceType
}

export enum ConstraintsSchemaType {
  Xml,
  Json
}

export enum ItemChoiceType {
  Inline,
  Url
}

export enum BasicType {
  String,
  Integer,
  Real,
  Boolean,
  Binary,
  Date,
  Time,
  Timestamp,
  Any
}

export interface ListType {
  dataType: DataTypeType;
}

export interface StructureType {
  element: SiLAElement[];
}

export interface StateUpdate {
  progress: number,
  estimatedRemainingTime: Date,
  state: CommandState
}

export enum CommandState {
  NotStarted,
  Running,
  FinishedWithErrors,
  FinishedSuccess
}