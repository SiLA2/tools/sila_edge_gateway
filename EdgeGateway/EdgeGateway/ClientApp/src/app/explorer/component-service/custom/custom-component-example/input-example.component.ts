import { Component } from '@angular/core';
import { InputComponent } from '../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty } from 'src/generated-sources/openapi';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BasicType, ConstrainedType, Constraints } from '../../interfaces';

@Component({
  selector: 'app-input-example',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  templateUrl: './input-example.component.html',
  styleUrl: './input-example.component.css'
})
export class InputExampleComponent implements InputComponent {
  options = ["Option 1", "Option 2", "Option 3"];

  selectedOption = "";

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, structure: Object, constraints?: Constraints) {
    if (constraints?.set) {
      this.options = constraints.set;

    } else {
      try {
        if ("constraints" in structure) {
          var constrainedStructure = (structure as ConstrainedType);
          if (constrainedStructure.constraints.set) {
            this.options = constrainedStructure.constraints.set;
          }
        }
      } catch {}
    }
  }

  setValue(value: string) {
    this.selectedOption = value;
  }

  getValue(): string {
    return this.selectedOption;
  }
}

