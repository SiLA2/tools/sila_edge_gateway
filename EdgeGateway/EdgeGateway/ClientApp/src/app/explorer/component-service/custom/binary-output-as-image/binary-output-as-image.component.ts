import { Component, OnInit } from '@angular/core';
import { BinaryOutputComponent } from '../../default/output/basic-output/binary-output/binary-output.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-binary-output-as-image',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './binary-output-as-image.component.html',
  styleUrl: './binary-output-as-image.component.css'
})
export class BinaryOutputAsImageComponent extends BinaryOutputComponent {
  imageSource?: string;

  override ngOnInit() {
    this.blob.subscribe(blob => {
      var reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onload = () => {
        this.imageSource = reader.result?.toString();
      };
    });
  }
}
