import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryOutputAsImageComponent } from './binary-output-as-image.component';

describe('BinaryOutputAsImageComponent', () => {
  let component: BinaryOutputAsImageComponent;
  let fixture: ComponentFixture<BinaryOutputAsImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BinaryOutputAsImageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BinaryOutputAsImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
