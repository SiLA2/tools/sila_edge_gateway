import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryInputComponent } from './binary-input.component';

describe('BinaryInputComponent', () => {
  let component: BinaryInputComponent;
  let fixture: ComponentFixture<BinaryInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BinaryInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BinaryInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
