import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyOutputComponent } from './any-output.component';

describe('AnyOutputComponent', () => {
  let component: AnyOutputComponent;
  let fixture: ComponentFixture<AnyOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AnyOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnyOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
