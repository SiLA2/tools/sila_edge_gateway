import { ChangeDetectorRef, Component, ComponentRef, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { ComponentService, InputComponent } from '../../../component.service';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { FeatureDescription, FeatureCommand, FeatureProperty, SiLAElement } from 'src/generated-sources/openapi';
import { StructureType } from '../../../interfaces';

@Component({
  selector: 'app-structure-input',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './structure-input.component.html',
  styleUrl: './structure-input.component.css'
})
export class StructureInputComponent implements InputComponent {
  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<InputComponent>[] = [];

  feature!: FeatureDescription;
  contextIdentifier!: string;
  elements?: SiLAElement[];

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: StructureType) {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.elements = structure.element;

    this.changeDetectorRef.detectChanges();
    this.createInputComponents();
  }

  private createInputComponents() {
    this.elements?.forEach((element, index) => {
      var componentType = this.componentService.getInputComponent(element.identifier!, this.contextIdentifier, element.dataType!);
      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, this.contextIdentifier, element.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  setValue(value: {[key: string]: Object}): void {
    this.elements?.forEach((element, index) => {
      var elementValue = value[element.identifier!];
      this.componentRefs.at(index)!.instance.setValue(elementValue);
    });
  }

  getValue(): {[key: string]: Object} {
    var result: {[key: string]: Object} = {};

    this.elements?.forEach((element, index) => {
      var elementValue = this.componentRefs.at(index)!.instance.getValue();
      result[element.identifier!] = elementValue;
    });

    return result;
  }
}
