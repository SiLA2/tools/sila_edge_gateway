import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { InputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-integer-input',
  standalone: true,
  imports: [
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule
  ],
  templateUrl: './integer-input.component.html',
  styleUrl: './integer-input.component.css'
})
export class IntegerInputComponent implements InputComponent {
  inputValue = new FormControl();

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: Object) {
    this.inputValue.setValue(value);
  }

  getValue(): Object {
    return this.inputValue.value;
  }
}
