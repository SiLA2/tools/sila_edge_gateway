import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureInputComponent } from './structure-input.component';

describe('StructureInputComponent', () => {
  let component: StructureInputComponent;
  let fixture: ComponentFixture<StructureInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StructureInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StructureInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
