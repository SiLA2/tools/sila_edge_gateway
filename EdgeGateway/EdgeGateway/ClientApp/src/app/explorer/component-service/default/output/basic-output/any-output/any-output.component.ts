import { Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, OutputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { DataTypeType, DynamicObjectProperty, FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-any-output',
  standalone: true,
  imports: [
    
  ],
  templateUrl: './any-output.component.html',
  styleUrl: './any-output.component.css'
})
export class AnyOutputComponent implements OutputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<OutputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;

  constructor(
    private componentService: ComponentService
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, _structure: Object, _constraints?: Constraints) {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
  }

  setValue(dynamicObjectProperty: DynamicObjectProperty) {
    this.createOutputComponent(dynamicObjectProperty.identifier!, dynamicObjectProperty.type!);
    this.componentRef.instance.setValue(dynamicObjectProperty.value);
  }

  private createOutputComponent(identifier: string, dataType: DataTypeType) {
    var index = this.viewContainerRef.indexOf(this.componentRef?.hostView!);
    if (index != -1) this.viewContainerRef.remove(index);

    var componentType = this.componentService.getOutputComponent(identifier, this.contextIdentifier, dataType);

    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, dataType.item);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }
}
