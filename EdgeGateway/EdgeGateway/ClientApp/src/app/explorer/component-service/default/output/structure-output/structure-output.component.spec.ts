import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureOutputComponent } from './structure-output.component';

describe('StructureOutputComponent', () => {
  let component: StructureOutputComponent;
  let fixture: ComponentFixture<StructureOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StructureOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StructureOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
