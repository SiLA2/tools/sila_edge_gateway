import { ChangeDetectorRef, Component, ComponentRef, QueryList, Type, ViewChildren, ViewContainerRef } from '@angular/core';
import { ComponentService, InputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, DataTypeType } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ListType } from '../../../interfaces';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-list-input',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule
  ],
  templateUrl: './list-input.component.html',
  styleUrl: './list-input.component.css'
})
export class ListInputComponent implements InputComponent {
  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<InputComponent>[] = [];
  itemCounter: number = 0;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  dataType!: DataTypeType;
  componentType!: Type<InputComponent>;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: ListType) {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.dataType = structure.dataType;
    this.componentType = this.componentService.getDefaultInputComponent(structure.dataType);
  }

  addListItem() {
    this.itemCounter++;
    this.changeDetectorRef.detectChanges();

    var componentRef = this.viewContainerRefs.get(this.itemCounter - 1)!.createComponent(this.componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.item);
    componentRef.changeDetectorRef.detectChanges();

    this.componentRefs.push(componentRef);
  }

  removeListItem() {
    if (this.itemCounter == 0) return;
    this.itemCounter--;
    this.componentRefs.pop();
  }

  setValue(values: Object[]) {
    while (this.itemCounter < values.length) {
      this.addListItem();
    }

    while (this.itemCounter > values.length) {
      this.removeListItem();
    }

    values.forEach((value, index) => {
      this.componentRefs.at(index)!.instance.setValue(value);
    });
  }

  getValue(): Object[] {
    var result: Object[] = [];

    this.componentRefs.forEach(componentRef => {
      var itemValue = componentRef.instance.getValue();
      result.push(itemValue);
    });

    return result;
  }
}
