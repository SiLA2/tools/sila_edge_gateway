import { Component, EventEmitter, Output, forwardRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { TypeSelectorComponent } from '../type-selector/type-selector.component';
import { ListType } from 'src/app/explorer/component-service/interfaces';
import { DataTypeType } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-list-type-selector',
  standalone: true,
  imports: [
    MatCardModule,
    forwardRef(() => TypeSelectorComponent)
  ],
  templateUrl: './list-type-selector.component.html',
  styleUrl: './list-type-selector.component.css'
})
export class ListTypeSelectorComponent {
  @Output() dataTypeChange = new EventEmitter<ListType>();

  emitListDataType(dataType: DataTypeType) {
    if (dataType) {
      this.dataTypeChange.emit({ dataType: dataType });
    } else {
      this.dataTypeChange.emit();
    }
  }
}
