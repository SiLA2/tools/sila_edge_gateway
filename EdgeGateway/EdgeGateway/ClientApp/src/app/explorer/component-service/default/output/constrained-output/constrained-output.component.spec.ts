import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstrainedOutputComponent } from './constrained-output.component';

describe('ConstrainedOutputComponent', () => {
  let component: ConstrainedOutputComponent;
  let fixture: ComponentFixture<ConstrainedOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConstrainedOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConstrainedOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
