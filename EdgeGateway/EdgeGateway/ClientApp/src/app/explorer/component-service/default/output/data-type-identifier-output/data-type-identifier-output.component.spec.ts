import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTypeIdentifierOutputComponent } from './data-type-identifier-output.component';

describe('DataTypeIdentifierOutputComponent', () => {
  let component: DataTypeIdentifierOutputComponent;
  let fixture: ComponentFixture<DataTypeIdentifierOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DataTypeIdentifierOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DataTypeIdentifierOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
