import { ChangeDetectorRef, Component, ComponentRef, QueryList, Type, ViewChildren, ViewContainerRef } from '@angular/core';
import { ComponentService, OutputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, DataTypeType } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { ListType } from '../../../interfaces';

@Component({
  selector: 'app-list-output',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './list-output.component.html',
  styleUrl: './list-output.component.css'
})
export class ListOutputComponent implements OutputComponent {
  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<OutputComponent>[] = [];
  itemCounter: number = 0;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  dataType!: DataTypeType;
  componentType!: Type<OutputComponent>;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: ListType): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.dataType = structure.dataType;
    this.componentType = this.componentService.getDefaultOutputComponent(structure.dataType);
  }

  addListItem() {
    this.itemCounter++;
    this.changeDetectorRef.detectChanges();

    var componentRef = this.viewContainerRefs.get(this.itemCounter - 1)!.createComponent(this.componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.item);
    componentRef.changeDetectorRef.detectChanges();

    this.componentRefs.push(componentRef);
  }

  removeListItem() {
    if (this.itemCounter == 0) return;
    this.itemCounter--;
    this.componentRefs.pop();
  }

  setValue(values: Object[] | null) {
    if (values == null) values = [];

    while (this.itemCounter < values.length) {
      this.addListItem();
    }

    while (this.itemCounter > values.length) {
      this.removeListItem();
    }

    values.forEach((value, index) => {
      this.componentRefs.at(index)!.instance.setValue(value);
    });
  }
}
