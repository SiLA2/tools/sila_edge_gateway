import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { InputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-boolean-input',
  standalone: true,
  imports: [
    MatSlideToggle,
    ReactiveFormsModule
  ],
  templateUrl: './boolean-input.component.html',
  styleUrl: './boolean-input.component.css'
})
export class BooleanInputComponent implements InputComponent {
  inputValue = new FormControl(false);

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: Object) {
    this.inputValue.setValue(value as boolean);
  }

  getValue(): Object {
    return this.inputValue.value!;
  }
}
