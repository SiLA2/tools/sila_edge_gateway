import { ChangeDetectorRef, Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, InputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { DataTypeType, DynamicObjectProperty, FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';
import { TypeSelectorComponent } from './type-selector/type-selector.component';
import { MatDividerModule } from '@angular/material/divider';


@Component({
  selector: 'app-any-input',
  standalone: true,
  imports: [
    CommonModule,
    MatDividerModule,
    TypeSelectorComponent
  ],
  templateUrl: './any-input.component.html',
  styleUrl: './any-input.component.css'
})
export class AnyInputComponent implements InputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef?: ComponentRef<InputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  allowedTypes?: DataTypeType[];
  dataType?: DataTypeType;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, _structure: Object, constraints?: Constraints) {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.allowedTypes = constraints?.allowedTypes;

    this.changeDetectorRef.detectChanges();
  }

  setValue(value: Object) {
    this.componentRef?.instance.setValue(value);
  }

  getValue(): DynamicObjectProperty {
    return {
      type: this.dataType,
      value: this.componentRef?.instance.getValue()
    };
  }

  createInputComponent(dataType: DataTypeType) {
    var index = this.viewContainerRef.indexOf(this.componentRef?.hostView!);
    if (index != -1) this.viewContainerRef.remove(index);

    this.dataType = dataType;
    if (!dataType) return;

    var componentType = this.componentService.getDefaultInputComponent(dataType);
    this.componentRef = this.viewContainerRef.createComponent(componentType);
    this.componentRef.instance.setStructure(this.feature, this.contextIdentifier, dataType.item);
    this.componentRef.changeDetectorRef.detectChanges();
  }
}
