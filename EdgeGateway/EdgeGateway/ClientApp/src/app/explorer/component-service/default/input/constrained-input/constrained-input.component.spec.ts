import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstrainedInputComponent } from './constrained-input.component';

describe('ConstrainedInputComponent', () => {
  let component: ConstrainedInputComponent;
  let fixture: ComponentFixture<ConstrainedInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ConstrainedInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConstrainedInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
