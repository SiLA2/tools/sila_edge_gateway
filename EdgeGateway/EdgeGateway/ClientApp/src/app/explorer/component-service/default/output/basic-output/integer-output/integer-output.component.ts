import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { OutputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-integer-output',
  standalone: true,
  imports: [
    MatCardModule
  ],
  templateUrl: './integer-output.component.html',
  styleUrl: './integer-output.component.css'
})
export class IntegerOutputComponent implements OutputComponent {
  outputValue?: Object;

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: Object) {
    this.outputValue = value;
  }
}

