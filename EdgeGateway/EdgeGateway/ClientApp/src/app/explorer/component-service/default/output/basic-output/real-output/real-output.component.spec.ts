import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealOutputComponent } from './real-output.component';

describe('RealOutputComponent', () => {
  let component: RealOutputComponent;
  let fixture: ComponentFixture<RealOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RealOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RealOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
