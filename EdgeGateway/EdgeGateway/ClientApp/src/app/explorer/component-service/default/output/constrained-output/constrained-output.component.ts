import { ChangeDetectorRef, Component, ComponentRef, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, OutputComponent } from '../../../component.service';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { DataTypeType, FeatureCommand, FeatureDescription, FeatureProperty, SiLAElement } from 'src/generated-sources/openapi';
import { ConstrainedType, Constraints } from '../../../interfaces';

@Component({
  selector: 'app-constrained-output',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './constrained-output.component.html',
  styleUrl: './constrained-output.component.css'
})
export class ConstrainedOutputComponent implements OutputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<OutputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  constraints!: Constraints
  dataType!: DataTypeType;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: ConstrainedType): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.constraints = structure.constraints;
    this.dataType = structure.dataType;

    this.changeDetectorRef.detectChanges();
    this.createOutputComponent();
  }

  private createOutputComponent() {
    var componentType: Type<OutputComponent>;
    if (this.constraints.contentType) {
      var mimeType = `${this.constraints.contentType.type}/${this.constraints.contentType.subtype}`;
      componentType = this.componentService.getOutputComponent(mimeType, this.contextIdentifier, this.dataType);

    } else {
      componentType = this.componentService.getDefaultOutputComponent(this.dataType);
    }

    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.item, this.constraints);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }

  setValue(value: Object | null) {
    this.componentRef.instance.setValue(value);
  }

  stringifyDataType(dataType: DataTypeType): string {
    return JSON.stringify(dataType);
  }
}
