import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { OutputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-real-output',
  standalone: true,
  imports: [
    MatCardModule
  ],
  templateUrl: './real-output.component.html',
  styleUrl: './real-output.component.css'
})
export class RealOutputComponent implements OutputComponent {
  outputValue?: Object;

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: Object) {
    this.outputValue = value;
  }
}
