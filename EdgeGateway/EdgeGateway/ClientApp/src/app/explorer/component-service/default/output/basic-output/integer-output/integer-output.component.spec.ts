import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegerOutputComponent } from './integer-output.component';

describe('IntegerOutputComponent', () => {
  let component: IntegerOutputComponent;
  let fixture: ComponentFixture<IntegerOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IntegerOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IntegerOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
