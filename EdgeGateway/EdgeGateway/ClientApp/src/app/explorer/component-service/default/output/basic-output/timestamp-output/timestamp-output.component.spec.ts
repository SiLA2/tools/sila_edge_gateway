import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimestampOutputComponent } from './timestamp-output.component';

describe('TimestampOutputComponent', () => {
  let component: TimestampOutputComponent;
  let fixture: ComponentFixture<TimestampOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TimestampOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TimestampOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
