import { CommonModule } from '@angular/common';
import { HttpEventType } from '@angular/common/http';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { InputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { BinariesService, FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@Component({
  selector: 'app-binary-input',
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatProgressBarModule
  ],
  templateUrl: './binary-input.component.html',
  styleUrl: './binary-input.component.css'
})
export class BinaryInputComponent implements InputComponent {
  value?: string;
  valueType?: ValueType
  uploadProgress = 0;

  constructor(
    private binariesService: BinariesService
  ) {}

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: string) {
    this.value = value;
  }

  getValue(): string {
    if (!this.value) throw new Error("File not yet loaded");
    return this.value;
  }

  async onFileSelected(event: Event) {
    var file = (event.target as HTMLInputElement).files![0];

    this.uploadProgress = 0;
    if (file.size < FILE_SIZE_THRESHOLD) {
      this.valueType = ValueType.Base64;
      this.encodeBinaries(file);
    } else {
      this.valueType = ValueType.Identifier;
      this.uploadBinaries(file);
    }
  }

  private encodeBinaries(file: File) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      var dataUrl = reader.result?.toString();
      this.value = dataUrl?.slice(dataUrl.indexOf(",") + 1);
      this.uploadProgress = 100;
    };
  }

  private uploadBinaries(file: File) {
    var blob = new Blob([file], {type: file.type});
    this.binariesService.apiBinaryPut(blob, "events", true).subscribe(event => {
      switch (event.type) {
        case HttpEventType.UploadProgress: {
          if (event.total) this.uploadProgress = Math.round(100 * event.loaded / event.total);
          break;
        };
        case HttpEventType.Response: {
          this.value = event.body || undefined;
          break;
        }
      }
    });
  }
}

const FILE_SIZE_THRESHOLD = 200_000;

enum ValueType {
  Base64,
  Identifier
}
