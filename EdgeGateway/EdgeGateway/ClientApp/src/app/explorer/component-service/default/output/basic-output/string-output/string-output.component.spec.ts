import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StringOutputComponent } from './string-output.component';

describe('StringOutputComponent', () => {
  let component: StringOutputComponent;
  let fixture: ComponentFixture<StringOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StringOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StringOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
