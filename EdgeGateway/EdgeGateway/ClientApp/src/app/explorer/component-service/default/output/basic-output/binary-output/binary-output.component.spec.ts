import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryOutputComponent } from './binary-output.component';

describe('BinaryOutputComponent', () => {
  let component: BinaryOutputComponent;
  let fixture: ComponentFixture<BinaryOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BinaryOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BinaryOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
