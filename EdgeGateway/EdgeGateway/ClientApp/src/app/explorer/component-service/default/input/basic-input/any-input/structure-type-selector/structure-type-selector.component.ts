import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output, forwardRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DataTypeType, SiLAElement } from 'src/generated-sources/openapi';
import { TypeSelectorComponent } from '../type-selector/type-selector.component';
import { StructureType } from 'src/app/explorer/component-service/interfaces';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-structure-type-selector',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    FormsModule,
    forwardRef(() => TypeSelectorComponent)
  ],
  templateUrl: './structure-type-selector.component.html',
  styleUrl: './structure-type-selector.component.css'
})
export class StructureTypeSelectorComponent {
  @Output() dataTypeChange = new EventEmitter<StructureType>();

  elements: SiLAElement[] = [];

  addStructureElement() {
    this.elements.push({
      identifier: "",
      displayName: "",
      description: "",
      dataType: undefined
    });
  }

  removeStructureElement() {
    this.elements.pop();
    this.emitStructureDataType();
  }

  emitStructureDataType() {
    for (var element of this.elements) {
      if (element.dataType == undefined) {
        this.dataTypeChange.emit();
        return;
      }
    }

    this.dataTypeChange.emit({element: this.elements});
  }
}
