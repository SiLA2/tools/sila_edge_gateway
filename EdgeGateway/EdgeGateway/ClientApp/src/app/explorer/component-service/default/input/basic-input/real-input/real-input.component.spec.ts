import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealInputComponent } from './real-input.component';

describe('RealInputComponent', () => {
  let component: RealInputComponent;
  let fixture: ComponentFixture<RealInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RealInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RealInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
