import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyInputComponent } from './any-input.component';

describe('AnyInputComponent', () => {
  let component: AnyInputComponent;
  let fixture: ComponentFixture<AnyInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AnyInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AnyInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
