import { ChangeDetectorRef, Component, ComponentRef, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, InputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, SiLAElement, DataTypeType } from 'src/generated-sources/openapi';
import { ConstrainedType, Constraints } from '../../../interfaces';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-constrained-input',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './constrained-input.component.html',
  styleUrl: './constrained-input.component.css'
})
export class ConstrainedInputComponent implements InputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<InputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  constraints!: Constraints
  dataType!: DataTypeType;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: ConstrainedType): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.constraints = structure.constraints;
    this.dataType = structure.dataType;

    this.changeDetectorRef.detectChanges();
    this.createInputComponent();
  }

  private createInputComponent() {
    var componentType: Type<InputComponent>;
    if (this.constraints.contentType) {
      var mimeType = `${this.constraints.contentType.type}/${this.constraints.contentType.subtype}`;
      componentType = this.componentService.getInputComponent(mimeType, this.contextIdentifier, this.dataType);

    } else {
      componentType = this.componentService.getDefaultInputComponent(this.dataType);
    }

    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.item, this.constraints);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }

  setValue(value: Object) {
    this.componentRef.instance.setValue(value);
  }

  getValue(): Object {
    return this.componentRef.instance.getValue();
  }

  stringifyDataType(dataType: DataTypeType): string {
    return JSON.stringify(dataType);
  }
}
