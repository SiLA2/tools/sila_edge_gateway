import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTypeIdentifierInputComponent } from './data-type-identifier-input.component';

describe('DataTypeIdentifierInputComponent', () => {
  let component: DataTypeIdentifierInputComponent;
  let fixture: ComponentFixture<DataTypeIdentifierInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DataTypeIdentifierInputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DataTypeIdentifierInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
