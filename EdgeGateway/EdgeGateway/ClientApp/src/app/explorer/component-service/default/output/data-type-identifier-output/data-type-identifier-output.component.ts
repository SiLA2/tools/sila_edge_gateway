import { ChangeDetectorRef, Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, OutputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, SiLAElement } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-data-type-identifier-output',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './data-type-identifier-output.component.html',
  styleUrl: './data-type-identifier-output.component.css'
})
export class DataTypeIdentifierOutputComponent implements OutputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<OutputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  dataType!: SiLAElement;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, dataTypeIdentifier: string): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.dataType = feature.dataTypes?.find(dataType => dataType.identifier == dataTypeIdentifier)!;

    this.changeDetectorRef.detectChanges();
    this.createOutputComponent();
  }

  private createOutputComponent() {
    var componentType = this.componentService.getOutputComponent(this.dataType.identifier!, this.contextIdentifier, this.dataType.dataType!);
    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.dataType?.item);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }

  setValue(value: Object | null) {
    this.componentRef.instance.setValue(value);
  }
}
