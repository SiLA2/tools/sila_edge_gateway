import { ChangeDetectorRef, Component, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentService, InputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, SiLAElement } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-data-type-identifier-input',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './data-type-identifier-input.component.html',
  styleUrl: './data-type-identifier-input.component.css'
})
export class DataTypeIdentifierInputComponent implements InputComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef!: ComponentRef<InputComponent>;

  feature!: FeatureDescription;
  contextIdentifier!: string;
  dataType!: SiLAElement;

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, dataTypeIdentifier: string): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.dataType = feature.dataTypes?.find(dataType => dataType.identifier == dataTypeIdentifier)!;

    this.changeDetectorRef.detectChanges();
    this.createInputComponent();
  }

  private createInputComponent() {
    var componentType = this.componentService.getInputComponent(this.dataType.identifier!, this.contextIdentifier, this.dataType.dataType!);
    var componentRef = this.viewContainerRef.createComponent(componentType);
    componentRef.instance.setStructure(this.feature, this.contextIdentifier, this.dataType.dataType?.item);
    componentRef.changeDetectorRef.detectChanges();
    this.componentRef = componentRef;
  }

  setValue(value: Object) {
    this.componentRef.instance.setValue(value);
  }

  getValue(): Object {
    return this.componentRef.instance.getValue();
  }
}
