import { CommonModule } from '@angular/common';
import { HttpEventType } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { Subject } from 'rxjs';
import { OutputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { BASE_PATH, BinariesService, FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-binary-output',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatProgressBarModule
  ],
  templateUrl: './binary-output.component.html',
  styleUrl: './binary-output.component.css'
})
export class BinaryOutputComponent implements OutputComponent, OnInit {
  blob: Subject<Blob> = new Subject();
  fileUrl?: string;
  mimeType?: string;

  downloadProgress = 0;

  constructor(
    private binariesService: BinariesService
  ) {}

  ngOnInit() {
    this.blob.subscribe(blob => {
      this.fileUrl = URL.createObjectURL(blob);
    });
  }

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, constraints?: Constraints) {
    if (constraints?.contentType) {
      this.mimeType = `${constraints.contentType.type}/${constraints.contentType.subtype}`;
    }
  }

  setValue(binaries: {kind: string}) {
    if (binaries.kind == "small") {
      this.decodeBase64((binaries as {kind: string, value: string}).value);

    } else if (binaries.kind == "large") {
      this.downloadBinaries((binaries as {kind: string, identifier: string}).identifier);

    } else throw new Error(`Unknown kind for binaries: ${binaries.kind}. Expected 'small' or 'large'`);
  }

  private decodeBase64(base64: string) {
    fetch(`data:${this.mimeType || "application/octet-stream"};base64,${base64}`)
    .then(response => response.blob())
    .then(blob => {
      this.blob.next(new Blob([blob], {type: this.mimeType || blob.type}));
    });
  }

  private downloadBinaries(fileUuid: string) {
    this.binariesService.apiBinaryGet(fileUuid, "events", true, {httpHeaderAccept: ("blob" as unknown)} as {httpHeaderAccept?: undefined}).subscribe(event => {
      switch (event.type) {
        case HttpEventType.DownloadProgress: {
          if (event.total) this.downloadProgress = Math.round(100 * event.loaded / event.total);
          break;
        };
        case HttpEventType.Response: {
          var blob: Blob = event.body;
          this.blob.next(new Blob([blob], {type: this.mimeType || blob.type}));
          break;
        }
      }
    });
  }

  downloadFile() {
    open(this.fileUrl);
  }
}
