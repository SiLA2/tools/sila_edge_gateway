import { ChangeDetectorRef, Component, ComponentRef, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { ComponentService, OutputComponent } from '../../../component.service';
import { FeatureDescription, FeatureCommand, FeatureProperty, SiLAElement, DynamicObjectProperty } from 'src/generated-sources/openapi';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { StructureType } from '../../../interfaces';

@Component({
  selector: 'app-structure-output',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './structure-output.component.html',
  styleUrl: './structure-output.component.css'
})
export class StructureOutputComponent implements OutputComponent {
  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<OutputComponent>[] = [];

  feature!: FeatureDescription;
  contextIdentifier!: string;
  elements?: SiLAElement[];

  constructor(
    private componentService: ComponentService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: StructureType): void {
    this.feature = feature;
    this.contextIdentifier = contextIdentifier;
    this.elements = structure.element;

    this.changeDetectorRef.detectChanges();
    this.createOutputComponents();
  }

  private createOutputComponents() {
    this.elements?.forEach((element, index) => {
      var componentType = this.componentService.getOutputComponent(element.identifier!, this.contextIdentifier, element.dataType!);
      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, this.contextIdentifier, element.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  setValue(value: {elements: DynamicObjectProperty[]} | null) {
    if (value == null) value = {
      elements: this.elements!.map(element => {
        var result: DynamicObjectProperty = {
          identifier: element.identifier,
          displayName: element.displayName,
          description: element.description,
          type: element.dataType,
          value: null
        }
        return result;
      }) || []
    };

    this.elements?.forEach((_element, index) => {
      var elementValue = value!.elements.at(index);
      this.componentRefs.at(index)!.instance.setValue(elementValue?.value);
    });
  }
}
