import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BooleanOutputComponent } from './boolean-output.component';

describe('BooleanOutputComponent', () => {
  let component: BooleanOutputComponent;
  let fixture: ComponentFixture<BooleanOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BooleanOutputComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BooleanOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
