import { CommonModule } from '@angular/common';
import { Component, ComponentRef, EventEmitter, Input, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule, MatExpansionPanel } from '@angular/material/expansion';
import { DataTypeType } from 'src/generated-sources/openapi';
import { ListTypeSelectorComponent } from '../list-type-selector/list-type-selector.component';
import { StructureTypeSelectorComponent } from '../structure-type-selector/structure-type-selector.component';
import { ListType, StructureType } from 'src/app/explorer/component-service/interfaces';

@Component({
  selector: 'app-type-selector',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatExpansionModule
  ],
  templateUrl: './type-selector.component.html',
  styleUrl: './type-selector.component.css'
})
export class TypeSelectorComponent {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef?: ComponentRef<ListTypeSelectorComponent | StructureTypeSelectorComponent>;

  @ViewChild(MatExpansionPanel) expansionPanel!: MatExpansionPanel;
  @Input() set allowedTypes(allowedTypes: DataTypeType[] | undefined) {
    if (!allowedTypes) return;
    this.types = allowedTypes.map((dataType, index) => {
      return {
        name: `Custom type ${index + 1}`,
        dataType: dataType
      };
    });
  };
  @Output() dataTypeChange = new EventEmitter<DataTypeType>();

  types: TypeOption[] = defaultTypeOptions;
  selectedType?: TypeOption;

  setSelectedType(type: TypeOption) {
    if (type == this.selectedType) return;
    this.selectedType = type;

    var index = this.viewContainerRef.indexOf(this.componentRef?.hostView!);
    if (index != -1) this.viewContainerRef.remove(index);

    this.dataTypeChange.emit();

    if (type.name == "List") {
      this.componentRef = this.viewContainerRef.createComponent(ListTypeSelectorComponent);
      this.componentRef.instance.dataTypeChange.subscribe((listType: ListType) => {
        if (listType) {
          this.dataTypeChange.emit({ item: listType });
        } else {
          this.dataTypeChange.emit();
        }
      });

    } else if (type.name == "Structure") {
      this.componentRef = this.viewContainerRef.createComponent(StructureTypeSelectorComponent);
      this.componentRef.instance.dataTypeChange.subscribe((structureType: StructureType) => {
        if (structureType) {
          this.dataTypeChange.emit({ item: structureType });
        } else {
          this.dataTypeChange.emit();
        }
      });

    } else {
      this.dataTypeChange.emit(type.dataType);
    }

    this.expansionPanel.close();
  }
}

interface TypeOption {
  name: string,
  dataType: DataTypeType
}

const defaultTypeOptions: TypeOption[] = [
  {
    name: "String",
    dataType: { item: 0 }
  },
  {
    name: "Integer",
    dataType: { item: 1 }
  },
  {
    name: "Real",
    dataType: { item: 2 }
  },
  {
    name: "Boolean",
    dataType: { item: 3 }
  },
  {
    name: "Binary",
    dataType: { item: 4 }
  },
  {
    name: "Date",
    dataType: { item: 5 }
  },
  {
    name: "Time",
    dataType: { item: 6 }
  },
  {
    name: "Timestamp",
    dataType: { item: 7 }
  },
  {
    name: "List",
    dataType: { item: { dataType: undefined } }
  },
  {
    name: "Structure",
    dataType: { item: { element: undefined } }
  }
];
