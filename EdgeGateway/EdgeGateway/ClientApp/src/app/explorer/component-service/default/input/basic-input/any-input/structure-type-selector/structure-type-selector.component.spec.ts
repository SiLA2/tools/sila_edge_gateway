import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureTypeSelectorComponent } from './structure-type-selector.component';

describe('StructureTypeSelectorComponent', () => {
  let component: StructureTypeSelectorComponent;
  let fixture: ComponentFixture<StructureTypeSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StructureTypeSelectorComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StructureTypeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
