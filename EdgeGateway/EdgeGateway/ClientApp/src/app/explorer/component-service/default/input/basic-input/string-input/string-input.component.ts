import { Component } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { InputComponent } from 'src/app/explorer/component-service/component.service';
import { Constraints } from 'src/app/explorer/component-service/interfaces';
import { FeatureDescription, FeatureCommand, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-string-input',
  standalone: true,
  imports: [
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule
  ],
  templateUrl: './string-input.component.html',
  styleUrl: './string-input.component.css'
})
export class StringInputComponent implements InputComponent {
  inputValue = new FormControl();

  setStructure(_feature: FeatureDescription, _contextIdentifier: string, _structure: Object, _constraints?: Constraints) {}

  setValue(value: Object) {
    this.inputValue.setValue(value);
  }

  getValue(): Object {
    return this.inputValue.value;
  }
}
