import { Injectable, Type } from '@angular/core';
import { DataTypeType, FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';
import { StructureInputComponent } from './default/input/structure-input/structure-input.component';
import { DataTypeIdentifierInputComponent } from './default/input/data-type-identifier-input/data-type-identifier-input.component';
import { StructureOutputComponent } from './default/output/structure-output/structure-output.component';
import { DataTypeIdentifierOutputComponent } from './default/output/data-type-identifier-output/data-type-identifier-output.component';
import { ListInputComponent } from './default/input/list-input/list-input.component';
import { ListOutputComponent } from './default/output/list-output/list-output.component';
import { ConstrainedInputComponent } from './default/input/constrained-input/constrained-input.component';
import { ConstrainedOutputComponent } from './default/output/constrained-output/constrained-output.component';
import { ComponentRegistry } from './component-registry';
import { Constraints } from './interfaces';
import { StringInputComponent } from './default/input/basic-input/string-input/string-input.component';
import { IntegerInputComponent } from './default/input/basic-input/integer-input/integer-input.component';
import { RealInputComponent } from './default/input/basic-input/real-input/real-input.component';
import { BooleanInputComponent } from './default/input/basic-input/boolean-input/boolean-input.component';
import { BinaryInputComponent } from './default/input/basic-input/binary-input/binary-input.component';
import { DateInputComponent } from './default/input/basic-input/date-input/date-input.component';
import { TimeInputComponent } from './default/input/basic-input/time-input/time-input.component';
import { TimestampInputComponent } from './default/input/basic-input/timestamp-input/timestamp-input.component';
import { AnyInputComponent } from './default/input/basic-input/any-input/any-input.component';
import { StringOutputComponent } from './default/output/basic-output/string-output/string-output.component';
import { IntegerOutputComponent } from './default/output/basic-output/integer-output/integer-output.component';
import { RealOutputComponent } from './default/output/basic-output/real-output/real-output.component';
import { BooleanOutputComponent } from './default/output/basic-output/boolean-output/boolean-output.component';
import { BinaryOutputComponent } from './default/output/basic-output/binary-output/binary-output.component';
import { DateOutputComponent } from './default/output/basic-output/date-output/date-output.component';
import { TimeOutputComponent } from './default/output/basic-output/time-output/time-output.component';
import { TimestampOutputComponent } from './default/output/basic-output/timestamp-output/timestamp-output.component';
import { AnyOutputComponent } from './default/output/basic-output/any-output/any-output.component';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {
  private componentRegistry: ComponentRegistry = new ComponentRegistry();

  getInputComponent(componentIdentifier: string, fullyQualifiedIdentifier: string, dataType: DataTypeType): Type<InputComponent> {
    return this.componentRegistry.getCustomInputComponent(componentIdentifier, fullyQualifiedIdentifier) || this.getDefaultInputComponent(dataType);
  }

  getDefaultInputComponent(dataType: DataTypeType): Type<InputComponent> {
    var item = dataType.item;

    switch (typeof item) {
      case "number": {
        switch (item) {
          case 0: return StringInputComponent;
          case 1: return IntegerInputComponent;
          case 2: return RealInputComponent;
          case 3: return BooleanInputComponent;
          case 4: return BinaryInputComponent;
          case 5: return DateInputComponent;
          case 6: return TimeInputComponent;
          case 7: return TimestampInputComponent;
          case 8: return AnyInputComponent;
          default: throw new Error("Unknown basic data type");
        }
      };
      case "string": return DataTypeIdentifierInputComponent;
      case "object": {
        if ("constraints" in item) return ConstrainedInputComponent;
        if ("dataType" in item) return ListInputComponent;
        if ("element" in item) return StructureInputComponent;
        throw new Error("Unknown object data type");
      }
      default: throw new Error("Unknown data type");
    }
  }

  getOutputComponent(componentIdentifier: string, fullyQualifiedIdentifier: string, dataType: DataTypeType): Type<OutputComponent> {
    return this.componentRegistry.getCustomOutputComponent(componentIdentifier, fullyQualifiedIdentifier) || this.getDefaultOutputComponent(dataType);
  }

  getDefaultOutputComponent(dataType: DataTypeType): Type<OutputComponent> {
    var item = dataType.item;

    switch (typeof item) {
      case "number": {
        switch (item) {
          case 0: return StringOutputComponent;
          case 1: return IntegerOutputComponent;
          case 2: return RealOutputComponent;
          case 3: return BooleanOutputComponent;
          case 4: return BinaryOutputComponent;
          case 5: return DateOutputComponent;
          case 6: return TimeOutputComponent;
          case 7: return TimestampOutputComponent;
          case 8: return AnyOutputComponent;
          default: throw new Error("Unknown basic data type");
        }
      }
      case "string": return DataTypeIdentifierOutputComponent;
      case "object": {
        if ("constraints" in item) return ConstrainedOutputComponent;
        if ("dataType" in item) return ListOutputComponent;
        if ("element" in item) return StructureOutputComponent;
        throw new Error("Unknown object data type");
      }
      default: throw new Error("Unknown data type");
    }
  }
}

export interface InputComponent {
  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: Object, constraints?: Constraints): void;
  setValue(value: Object): void;
  getValue(): Object;
}

export interface OutputComponent {
  setStructure(feature: FeatureDescription, contextIdentifier: string, structure: Object, constraints?: Constraints): void;
  setValue(value: Object | null): void;
}
