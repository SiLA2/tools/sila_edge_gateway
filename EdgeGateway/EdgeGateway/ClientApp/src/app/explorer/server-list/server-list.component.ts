import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { firstValueFrom } from 'rxjs';
import { GatewayService, ServerInfo } from 'src/generated-sources/openapi';
import { EditServerNameDialogComponent } from './edit-server-name-dialog/edit-server-name-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { APPLICATION_TYPE } from 'src/app/app.module';

@Component({
  selector: 'app-server-list',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatRippleModule,
    MatTooltipModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatChipsModule
  ],
  templateUrl: './server-list.component.html',
  styleUrl: './server-list.component.css'
})
export class ServerListComponent implements OnInit {
  servers?: ServerInfo[];
  selectedServer?: ServerInfo;
  @Output() selectedServerChange = new EventEmitter<ServerInfo>();

  constructor(
    @Inject(APPLICATION_TYPE) public applicationType: "EdgeGateway" | "ServerPool",
    private gatewayService: GatewayService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {}

  async ngOnInit() {
    await this.getServers();
  }

  async getServers() {
    var servers = await firstValueFrom(this.gatewayService.apiGatewayGet());
    this.servers = servers.sort((a, b) => a.serverName!.localeCompare(b.serverName!));
  }

  setSelectedServer(server: ServerInfo) {
    if (server == this.selectedServer) return;

    if (this.isGateway(server)) {
      this.snackbar.open("Direct interaction with features propagated by gateway is not possible. Select a connected server instead.", undefined, { duration: 5000 });
      return;
    }

    this.selectedServer = server;
    this.selectedServerChange.emit(server);
  }

  isGateway(server: ServerInfo): boolean {
    return server.features!.some(identifier => identifier == "org.silastandard/gateway/GatewayService/v1");
  }

  editServerName(server: ServerInfo) {
    var dialogRef = this.dialog.open(EditServerNameDialogComponent, {
      height: "300px",
      width: "400px",
      autoFocus: false,
      data: {
        server: server
      }
    });
  }
}
