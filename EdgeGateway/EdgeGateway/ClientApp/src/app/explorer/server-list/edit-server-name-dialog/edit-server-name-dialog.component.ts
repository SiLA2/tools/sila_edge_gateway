import { Component, Inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { firstValueFrom } from 'rxjs';
import { MiscellaneousService, ServerInfo } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-edit-server-name-dialog',
  standalone: true,
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    ReactiveFormsModule
  ],
  templateUrl: './edit-server-name-dialog.component.html',
  styleUrl: './edit-server-name-dialog.component.css'
})
export class EditServerNameDialogComponent {
  serverName = new FormControl();
  server: ServerInfo;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: {server: ServerInfo},
    private dialogRef: MatDialogRef<EditServerNameDialogComponent>,
    private miscellaneousService: MiscellaneousService,
    private snackbar: MatSnackBar
  ) {
    this.server = data.server;
    this.serverName.setValue(this.server.serverName);
  }

  async changeServerName() {
    await firstValueFrom(this.miscellaneousService.apiServernamePost({
      serverId: this.data.server.serveruuid,
      name: this.serverName.value
    }));

    this.snackbar.open(`Server name changed to '${this.serverName.value}'`, undefined, { duration: 3000 });

    this.dialogRef.close();
  }
}
