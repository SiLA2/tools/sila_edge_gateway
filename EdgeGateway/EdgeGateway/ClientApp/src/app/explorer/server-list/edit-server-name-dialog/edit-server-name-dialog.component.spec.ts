import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditServerNameDialogComponent } from './edit-server-name-dialog.component';

describe('EditServerNameDialogComponent', () => {
  let component: EditServerNameDialogComponent;
  let fixture: ComponentFixture<EditServerNameDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EditServerNameDialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EditServerNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
