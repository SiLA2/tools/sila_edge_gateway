import { Component, ComponentRef, Input, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { DynamicObjectProperty, FeatureCommand, FeatureDescription, SiLAElement } from 'src/generated-sources/openapi';
import { ComponentService, OutputComponent } from '../component-service/component.service';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-result-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './result-area.component.html',
  styleUrl: './result-area.component.css'
})
export class ResultAreaComponent {
  @Input({ required: true }) feature!: FeatureDescription;
  @Input({ required: true }) command!: FeatureCommand;

  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<OutputComponent>[] = [];

  fullyQualifiedIdentifier!: string;

  constructor(
    private componentService: ComponentService
  ) {}

  ngAfterViewInit() {
    this.fullyQualifiedIdentifier = `${this.feature.identifier}/Command/${this.command.identifier}`;
    this.createOutputComponents();
  }

  createOutputComponents() {
    this.command.response?.forEach((response, index) => {
      var componentType = this.componentService.getOutputComponent(response.identifier!, this.fullyQualifiedIdentifier, response.dataType!);

      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, this.fullyQualifiedIdentifier, response.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  setValue(value: {elements: DynamicObjectProperty[]}) {
    if (value == null) value = {
      elements: this.command.response?.map(response => {
        var result: DynamicObjectProperty = {
          identifier: response.identifier,
          displayName: response.displayName,
          description: response.description,
          type: response.dataType,
          value: null
        }
        return result;
      }) || []
    };

    this.command.response?.forEach((_response, index) => {
      var resultValue = value.elements.at(index);
      this.componentRefs.at(index)!.instance.setValue(resultValue?.value);
    });
  }
}
