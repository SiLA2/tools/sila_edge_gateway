import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediateValueAreaComponent } from './intermediate-value-area.component';

describe('IntermediateValueAreaComponent', () => {
  let component: IntermediateValueAreaComponent;
  let fixture: ComponentFixture<IntermediateValueAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IntermediateValueAreaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IntermediateValueAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
