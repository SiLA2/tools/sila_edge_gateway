import { CommonModule } from '@angular/common';
import { Component, ComponentRef, Input, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { DynamicObjectProperty, FeatureCommand, FeatureDescription } from 'src/generated-sources/openapi';
import { ComponentService, OutputComponent } from '../component-service/component.service';

@Component({
  selector: 'app-intermediate-value-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './intermediate-value-area.component.html',
  styleUrl: './intermediate-value-area.component.css'
})
export class IntermediateValueAreaComponent {
  @Input({ required: true }) feature!: FeatureDescription;
  @Input({ required: true }) command!: FeatureCommand;

  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<OutputComponent>[] = [];

  fullyQualifiedIdentifier!: string;

  constructor(
    private componentService: ComponentService
  ) {}

  ngAfterViewInit() {
    this.fullyQualifiedIdentifier = `${this.feature.identifier}/Command/${this.command.identifier}`;
    this.createOutputComponents();
  }

  createOutputComponents() {
    this.command.intermediateResponse?.forEach((intermediateResponse, index) => {
      var componentType = this.componentService.getOutputComponent(intermediateResponse.identifier!, this.fullyQualifiedIdentifier, intermediateResponse.dataType!);
      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, this.fullyQualifiedIdentifier, intermediateResponse.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  setValue(intermediateResponse: DynamicObjectProperty) {
    var intermediates = intermediateResponse.value.elements as DynamicObjectProperty[];
    this.command.intermediateResponse?.forEach((_intermediateResponse, index) => {
      this.componentRefs.at(index)!.instance.setValue(intermediates[index].value);
    });
  }
}
