import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FeatureCommand, FeatureDescription, FeatureProperty } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-command-property-list',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatRippleModule,
    MatTooltipModule,
    MatListModule,
    MatDividerModule
  ],
  templateUrl: './command-property-list.component.html',
  styleUrl: './command-property-list.component.css'
})
export class CommandPropertyListComponent {
  private _selectedFeature?: FeatureDescription;
  get selectedFeature(): FeatureDescription | undefined {
    return this._selectedFeature;
  }
  @Input({required: true}) set selectedFeature(feature: FeatureDescription | undefined) {
    this._selectedFeature = feature;
    this.setSelectedItem(undefined);
    if (feature) this.sortItems(feature);
  }
  
  selectedItem?: FeatureCommand | FeatureProperty;
  @Output() selectedItemChange = new EventEmitter<FeatureCommand | FeatureProperty>();

  sortItems(feature: FeatureDescription) {
    feature.commands?.sort((a, b) => a.displayName!.localeCompare(b.displayName!));
    feature.properties?.sort((a, b) => a.displayName!.localeCompare(b.displayName!));
  }

  setSelectedItem(item: FeatureCommand | FeatureProperty | undefined) {
    if (item == this.selectedItem) return;
    this.selectedItem = item;
    this.selectedItemChange.emit(item);
  }
}
