import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandPropertyListComponent } from './command-property-list.component';

describe('CommandPropertyListComponent', () => {
  let component: CommandPropertyListComponent;
  let fixture: ComponentFixture<CommandPropertyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommandPropertyListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CommandPropertyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
