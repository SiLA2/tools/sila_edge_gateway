import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterAreaComponent } from './parameter-area.component';

describe('ParameterAreaComponent', () => {
  let component: ParameterAreaComponent;
  let fixture: ComponentFixture<ParameterAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ParameterAreaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ParameterAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
