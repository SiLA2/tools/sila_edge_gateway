import { AfterViewInit, Component, ComponentRef, Input, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { FeatureCommand, FeatureDescription } from 'src/generated-sources/openapi';
import { ComponentService, InputComponent } from '../component-service/component.service';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-parameter-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './parameter-area.component.html',
  styleUrl: './parameter-area.component.css'
})
export class ParameterAreaComponent implements AfterViewInit {
  @Input({ required: true }) feature!: FeatureDescription;
  @Input({ required: true }) command!: FeatureCommand;

  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<InputComponent>[] = [];

  fullyQualifiedIdentifier!: string;

  constructor(
    private componentService: ComponentService
  ) {}

  ngAfterViewInit() {
    this.fullyQualifiedIdentifier = `${this.feature.identifier}/Command/${this.command.identifier}`;
    this.createInputComponents();
  }

  createInputComponents() {
    this.command.parameter?.forEach((parameter, index) => {
      var componentType = this.componentService.getInputComponent(parameter.identifier!, this.fullyQualifiedIdentifier, parameter.dataType!);
      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, this.fullyQualifiedIdentifier, parameter.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  getParameters(): {[key: string]: Object} {
    var result: {[key: string]: Object} = {};

    this.command.parameter?.forEach((parameter, index) => {
      var parameterValue = this.componentRefs.at(index)!.instance.getValue();
      result[parameter.identifier!] = parameterValue;
    });

    return result;
  }
}
