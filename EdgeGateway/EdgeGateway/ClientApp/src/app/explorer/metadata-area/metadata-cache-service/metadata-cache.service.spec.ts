import { TestBed } from '@angular/core/testing';

import { MetadataCacheService } from './metadata-cache.service';

describe('MetadataCacheService', () => {
  let service: MetadataCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetadataCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
