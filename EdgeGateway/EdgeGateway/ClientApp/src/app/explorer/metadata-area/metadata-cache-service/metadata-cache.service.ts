import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MetadataCacheService {
  private metadata: {[key: string]: Object};

  constructor() {
    this.metadata = JSON.parse(localStorage.getItem("metadata") || "{}");
  }

  storeMetadata(identifier: string, object: Object) {
    this.metadata[identifier] = object;
    localStorage.setItem("metadata", JSON.stringify(this.metadata));
  }

  getMetadata(identifier: string): Object | undefined {
    return this.metadata[identifier];
  }

  clearMetadata() {
    localStorage.removeItem("metadata");
    this.metadata = {};
  }
}
