import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataAreaComponent } from './metadata-area.component';

describe('MetadataAreaComponent', () => {
  let component: MetadataAreaComponent;
  let fixture: ComponentFixture<MetadataAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MetadataAreaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MetadataAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
