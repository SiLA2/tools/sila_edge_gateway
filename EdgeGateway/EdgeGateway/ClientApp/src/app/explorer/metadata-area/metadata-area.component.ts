import { CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentRef, Input, OnInit, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { ComponentService, InputComponent } from '../component-service/component.service';
import { FeatureCommand, FeatureDescription, FeatureMetadata, FeatureProperty, FeatureService, MetadataEntry, MetadataService } from 'src/generated-sources/openapi';
import { firstValueFrom, take } from 'rxjs';
import { MetadataCacheService } from './metadata-cache-service/metadata-cache.service';

@Component({
  selector: 'app-metadata-area',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule
  ],
  templateUrl: './metadata-area.component.html',
  styleUrl: './metadata-area.component.css'
})
export class MetadataAreaComponent implements OnInit, AfterViewInit {
  @Input({ required: true }) serverUuid!: string;
  @Input({ required: true }) feature!: FeatureDescription;
  @Input({ required: true }) item!: FeatureCommand | FeatureProperty;

  @ViewChildren("viewContainerRef", { read: ViewContainerRef }) viewContainerRefs!: QueryList<ViewContainerRef>;
  componentRefs: ComponentRef<InputComponent>[] = [];

  metadataIdentifiers: string[] = [];
  metadata: FeatureMetadata[] = [];

  constructor(
    private componentService: ComponentService,
    private metadataCacheService: MetadataCacheService,
    private metadataService: MetadataService,
    private featureService: FeatureService
  ) {}

  async ngOnInit() {
    await this.getMetadataIdentifiers();
    await this.getMetadataStructure();
  }

  ngAfterViewInit() {
    firstValueFrom(this.viewContainerRefs.changes).then(_ => {
      this.createInputComponents();
      this.readMetadataCache();
    });
  }

  private async getMetadataIdentifiers() {
    var featureIdentifier = this.feature.identifier!.replace(/\//g, "_");

    if ("parameter" in this.item) {
      // Item is a command
      var metadataIdentifiers = await firstValueFrom(this.metadataService.apiMetadataServerIdFeatureIdCommandsCommandIdGet(this.serverUuid, featureIdentifier, this.item.identifier!));
      this.metadataIdentifiers = metadataIdentifiers;
    } else {
      // Item is a property
      var metadataIdentifiers = await firstValueFrom(this.metadataService.apiMetadataServerIdFeatureIdPropertiesPropertyIdGet(this.serverUuid, featureIdentifier, this.item.identifier!));
      this.metadataIdentifiers = metadataIdentifiers;
    }
  }

  private async getMetadataStructure() {
    var result: FeatureMetadata[] = [];

    for (var identifier of this.metadataIdentifiers) {
      var match = identifier.match(/(?<featureIdentifier>.+)\/Metadata\/(?<metadataIdentifier>.+)/);
      var featureIdentifier = match?.groups!["featureIdentifier"].replace(/\//g, "_")!;
      var metadataIdentifier = match?.groups!["metadataIdentifier"]!;

      var metadataFeature = await firstValueFrom(this.featureService.apiFeatureIdentifierGet(featureIdentifier));
      var featureMetadata = metadataFeature.metadata?.find(metadata => metadata.identifier == metadataIdentifier)!;
      result.push(featureMetadata);
    };

    this.metadata = result;
  }

  private createInputComponents() {
    this.metadata.forEach((featureMetadata, index) => {
      var fullyQualifiedIdentifier = this.metadataIdentifiers[index];

      var componentType = this.componentService.getInputComponent(featureMetadata.identifier!, fullyQualifiedIdentifier, featureMetadata.dataType!);
      var componentRef = this.viewContainerRefs.get(index)!.createComponent(componentType);
      componentRef.instance.setStructure(this.feature, fullyQualifiedIdentifier, featureMetadata.dataType?.item);
      componentRef.changeDetectorRef.detectChanges();
      this.componentRefs.push(componentRef);
    });
  }

  private readMetadataCache() {
    this.metadataIdentifiers.forEach((identifier, index) => {
      var metadataValue = this.metadataCacheService.getMetadata(identifier);
      if (metadataValue) {
        this.componentRefs.at(index)!.instance.setValue(metadataValue);
      }
    });
  }

  getMetadata(): MetadataEntry[] {
    var result: MetadataEntry[] = [];

    this.metadata.forEach((_featureMetadata, index) => {
      var metadataValue = this.componentRefs.at(index)!.instance.getValue();
      var metadataEntry: MetadataEntry = {
        identifier: this.metadataIdentifiers.at(index),
        content: metadataValue
      }
      result.push(metadataEntry);

      this.writeMetadataCache(metadataEntry);
    });

    return result;
  }

  private writeMetadataCache(metadataEntry: MetadataEntry) {
    this.metadataCacheService.storeMetadata(metadataEntry.identifier!, metadataEntry.content);
  }
}
