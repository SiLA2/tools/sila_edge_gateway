import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnobservablePropertyComponent } from './unobservable-property.component';

describe('UnobservablePropertyComponent', () => {
  let component: UnobservablePropertyComponent;
  let fixture: ComponentFixture<UnobservablePropertyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UnobservablePropertyComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UnobservablePropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
