import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, Inject, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MetadataAreaComponent } from '../metadata-area/metadata-area.component';
import { FeatureDescription, FeatureProperty, PropertyRequestBody, PropertyService, ServerInfo } from 'src/generated-sources/openapi';
import { firstValueFrom } from 'rxjs';
import { PropertyValueAreaComponent } from '../property-value-area/property-value-area.component';
import { ErrorAreaComponent } from '../error-area/error-area.component';

@Component({
  selector: 'app-unobservable-property',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MetadataAreaComponent,
    PropertyValueAreaComponent,
    ErrorAreaComponent
  ],
  templateUrl: './unobservable-property.component.html',
  styleUrl: './unobservable-property.component.css'
})
export class UnobservablePropertyComponent implements AfterViewInit {
  @ViewChild(MetadataAreaComponent) metadataArea!: MetadataAreaComponent;
  @ViewChild(PropertyValueAreaComponent) propertyValueAreaComponent!: PropertyValueAreaComponent;
  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;

  propertyAreaVisible = false;
  errorAreaVisible = false;

  constructor(
    @Inject("server") public server: ServerInfo,
    @Inject("feature") public feature: FeatureDescription,
    @Inject("property") public property: FeatureProperty,

    private propertyService: PropertyService
  ) {}

  async ngAfterViewInit() {
    await new Promise(f => setTimeout(f, 100));
    this.getPropertyValue();
  }

  async getPropertyValue() {
    try {
      this.hideDisplayAreas();
      await this.getProperty();

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  private hideDisplayAreas() {
    this.propertyAreaVisible = false;
    this.errorAreaVisible = false;
  }

  private async getProperty() {
    var propertyRequestBody: PropertyRequestBody = {
      metadata: this.metadataArea.getMetadata()
    };

    var featureIdentifier = this.feature.identifier!.replace(/\//g, "_");

    var propertyValue = await firstValueFrom(
      this.propertyService.apiExecutionServerIdFeatureIdPropertiesPropertyIdPost(this.server.serveruuid!, featureIdentifier, this.property.identifier!, propertyRequestBody));

    this.propertyAreaVisible = true;
    this.propertyValueAreaComponent.setValue(propertyValue);
  }
}
