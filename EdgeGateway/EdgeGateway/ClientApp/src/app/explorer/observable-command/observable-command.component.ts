import { ChangeDetectorRef, Component, Inject, ViewChild } from '@angular/core';
import { CommandRequestBody, CommandResult, DynamicObjectProperty, FeatureCommand, FeatureDescription, ObservableCommandService, ServerInfo, UnobservableCommandService } from 'src/generated-sources/openapi';
import { ParameterAreaComponent } from '../parameter-area/parameter-area.component';
import { MatButtonModule } from '@angular/material/button';
import { MetadataAreaComponent } from '../metadata-area/metadata-area.component';
import { firstValueFrom } from 'rxjs';
import { ResultAreaComponent } from '../result-area/result-area.component';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { IntermediateValueAreaComponent } from '../intermediate-value-area/intermediate-value-area.component';
import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { StateUpdate } from '../component-service/interfaces';
import { DefinedExecutionError, ErrorAreaComponent, UndefinedExecutionError } from '../error-area/error-area.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-observable-command',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    ParameterAreaComponent,
    MetadataAreaComponent,
    IntermediateValueAreaComponent,
    ResultAreaComponent,
    ErrorAreaComponent,
    MatListModule,
    MatProgressBarModule
  ],
  templateUrl: './observable-command.component.html',
  styleUrl: './observable-command.component.css'
})
export class ObservableCommandComponent {
  @ViewChild(ParameterAreaComponent) parameterArea!: ParameterAreaComponent;
  @ViewChild(MetadataAreaComponent) metadataArea!: MetadataAreaComponent;
  @ViewChild(IntermediateValueAreaComponent) intermediateValueArea!: IntermediateValueAreaComponent;
  @ViewChild(ResultAreaComponent) resultAreaComponent!: ResultAreaComponent;
  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;

  resultAreaVisible = false;
  intermediateAreaVisible = false;
  errorAreaVisible = false;

  serverSupportsCancellation: boolean;

  hubConnection?: HubConnection;
  commandExecutionUuid?: string;

  commandState?: StateUpdate;

  constructor(
    @Inject("server") public server: ServerInfo,
    @Inject("feature") public feature: FeatureDescription,
    @Inject("command") public command: FeatureCommand,

    private observableCommandService: ObservableCommandService,
    private unobservableCommandService: UnobservableCommandService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.serverSupportsCancellation = server.features!.includes("org.silastandard/core.commands/CancelController/v1");
  }

  async executeObservableCommand() {
    try {
      this.hideDisplayAreas();

      this.commandExecutionUuid = await this.startCommandExecution();
      this.hubConnection = await this.createHubConnection();

      this.listenForStateUpdates();
      this.listenForIntermediates();
      this.listenForResult();

    } catch(error) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  private hideDisplayAreas() {
    this.resultAreaVisible = false;
    this.intermediateAreaVisible = false;
    this.errorAreaVisible = false;

    this.commandState = undefined;
  }

  private async startCommandExecution(): Promise<string> {
    var commandRequestBody: CommandRequestBody = {
      metadata: this.metadataArea.getMetadata(),
      parameters: this.parameterArea.getParameters()
    };

    var featureIdentifier = this.feature.identifier!.replace(/\//g, "_");

    var commandExecution = await firstValueFrom(
      this.observableCommandService.apiExecutionServerIdFeatureIdObservablecommandsCommandIdPost(this.server.serveruuid!, featureIdentifier, this.command.identifier!, commandRequestBody));

    if (commandExecution.errorMessage) throw new Error(commandExecution.errorMessage);

    return commandExecution.executionUUID!;
  }

  private async createHubConnection(): Promise<HubConnection> {
    var hubConnection = new HubConnectionBuilder()
    .withUrl("/hub", HttpTransportType.ServerSentEvents)
    .configureLogging(LogLevel.Trace)
    .build();

    await hubConnection.start();

    return hubConnection;
  }

  private listenForStateUpdates() {
    this.hubConnection?.on(`/commandexecution/${this.commandExecutionUuid}/state`, (stateUpdate: StateUpdate) => {
      this.commandState = stateUpdate;
      this.changeDetectorRef.detectChanges();
    });
  }

  private listenForIntermediates() {
    if (this.command.intermediateResponse != null) {
      this.hubConnection?.on(`/commandexecution/${this.commandExecutionUuid}/intermediates`, (intermediateResult: DynamicObjectProperty) => {
        this.intermediateAreaVisible = true;
        this.intermediateValueArea.setValue(intermediateResult);
        this.changeDetectorRef.detectChanges();
      });
    }
  }

  private listenForResult() {
    this.hubConnection?.on(`/commandexecution/${this.commandExecutionUuid}/result`, (commandResult: CommandResult) => {
      if (commandResult.definedError) throw new DefinedExecutionError(commandResult.definedError);
      if (commandResult.undefinedError) throw new UndefinedExecutionError(commandResult.undefinedError);
  
      this.resultAreaVisible = true;
      this.resultAreaComponent.setValue(commandResult.result?.value);
      this.changeDetectorRef.detectChanges();

      this.hubConnection?.stop();
      this.hubConnection = undefined;
    });
  }

  async cancelCommandExecution() {
    try {
      var commandRequestBody: CommandRequestBody = {
        metadata: [],
        parameters: {"CommandExecutionUUID": this.commandExecutionUuid}
      };
  
      var featureIdentifier = "org.silastandard/core.commands/CancelController/v1".replace(/\//g, "_");
      var commandIdentifier = "CancelCommand";
  
      await firstValueFrom(this.unobservableCommandService.apiExecutionServerIdFeatureIdCommandsCommandIdPost(this.server.serveruuid!, featureIdentifier, commandIdentifier, commandRequestBody));

    } catch(error) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }
}
