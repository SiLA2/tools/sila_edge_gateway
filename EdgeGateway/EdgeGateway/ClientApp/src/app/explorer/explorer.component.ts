import { Component } from '@angular/core';
import { FeatureCommand, FeatureDescription, FeatureProperty, ServerInfo } from 'src/generated-sources/openapi';
import { ServerListComponent } from './server-list/server-list.component';
import { FeatureListComponent } from './feature-list/feature-list.component';
import { CommandPropertyListComponent } from './command-property-list/command-property-list.component';
import { CommandPropertyAreaComponent } from './command-property-area/command-property-area.component';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css'],
  standalone: true,
  imports: [
    ServerListComponent,
    FeatureListComponent,
    CommandPropertyListComponent,
    CommandPropertyAreaComponent
  ]
})
export class ExplorerComponent {
  selectedServer?: ServerInfo;
  selectedFeature?: FeatureDescription;
  selectedItem?: FeatureCommand | FeatureProperty;
}
