import { Component, Inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { SettingsMenuComponent } from '../settings-menu/settings-menu.component';
import { APPLICATION_TYPE } from '../app.module';

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatDialogModule
  ],
  templateUrl: './toolbar.component.html',
  styleUrl: './toolbar.component.css'
})
export class ToolbarComponent {
  constructor(
    private dialog: MatDialog,
    @Inject(APPLICATION_TYPE) public applicationType: string
  ) {}

  openSettingsMenu() {
    var dialogRef = this.dialog.open(SettingsMenuComponent, {
      height: "700px",
      width: "1250px",
      autoFocus: false
    });
  }
}
