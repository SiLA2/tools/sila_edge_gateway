import { BrowserModule } from '@angular/platform-browser';
import { InjectionToken, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { ExplorerComponent } from './explorer/explorer.component';
import { provideAnimations } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ApiModule, BASE_PATH } from 'src/generated-sources/openapi';

import config from '../assets/config.json';

export const APPLICATION_TYPE = new InjectionToken<"EdgeGateway" | "ServerPool">('applicationType');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: ExplorerComponent, pathMatch: 'full' }
    ]),
    ApiModule,
    ToolbarComponent
  ],
  providers: [
    provideAnimations(),
    { provide: APPLICATION_TYPE, useValue: config.applicationType },
    { provide: BASE_PATH, useValue: config.apiBasePath }],
  bootstrap: [AppComponent]
})
export class AppModule { }
