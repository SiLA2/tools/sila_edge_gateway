import { CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentRef, Inject, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import { ServerConfigurationComponent } from './server-configuration/server-configuration.component';
import { ClientConfigurationComponent } from './client-configuration/client-configuration.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { APPLICATION_TYPE } from '../app.module';
import { AboutComponent } from './about/about.component';

@Component({
  selector: 'app-settings-menu',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatRippleModule,
    MatTooltipModule
  ],
  templateUrl: './settings-menu.component.html',
  styleUrl: './settings-menu.component.css'
})
export class SettingsMenuComponent implements AfterViewInit {
  @ViewChild("viewContainerRef", { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;
  componentRef?: ComponentRef<Component>;

  menuItems: MenuItem[] = [
    {
      label: "General",
      edgeGateway: true,
      serverPool: true,
      component: GeneralSettingsComponent
    },
    {
      label: "Server configuration",
      edgeGateway: true,
      serverPool: false,
      component: ServerConfigurationComponent
    },
    {
      label: "Client configuration",
      edgeGateway: true,
      serverPool: false,
      component: ClientConfigurationComponent
    },
    {
      label: "About",
      edgeGateway: true,
      serverPool: true,
      component: AboutComponent
    }
  ];

  selectedMenuItem?: MenuItem;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    @Inject(APPLICATION_TYPE) public applicationType: "EdgeGateway" | "ServerPool"
  ) {}

  ngAfterViewInit() {
    this.setSelectedMenuItem(this.menuItems[0]);
  }

  setSelectedMenuItem(menuItem: MenuItem) {
    if (this.selectedMenuItem == menuItem || !this.menuItemEnabled(menuItem)) return;

    this.selectedMenuItem = menuItem;
    this.changeDetectorRef.detectChanges();

    var index = this.viewContainerRef.indexOf(this.componentRef?.hostView!);
    if (index != -1) this.viewContainerRef.remove(index);

    this.componentRef = this.viewContainerRef.createComponent(menuItem.component);
    this.componentRef.changeDetectorRef.detectChanges();
  }

  menuItemEnabled(menuItem: MenuItem): boolean {
    return (this.applicationType == 'EdgeGateway' && menuItem.edgeGateway) || (this.applicationType == 'ServerPool' && menuItem.serverPool);
  }
}

interface MenuItem {
  label: string,
  edgeGateway: boolean,
  serverPool: boolean,
  component: any
}
