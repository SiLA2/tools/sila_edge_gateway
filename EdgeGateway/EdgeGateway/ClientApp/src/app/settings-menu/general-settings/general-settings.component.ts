import { CommonModule } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar } from '@angular/material/snack-bar';
import { firstValueFrom } from 'rxjs';
import { APPLICATION_TYPE } from 'src/app/app.module';
import { MetadataCacheService } from 'src/app/explorer/metadata-area/metadata-cache-service/metadata-cache.service';
import { GatewayService, MiscellaneousService } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-general-settings',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule
  ],
  templateUrl: './general-settings.component.html',
  styleUrl: './general-settings.component.css'
})
export class GeneralSettingsComponent implements OnInit {
  gatewayState?: GatewayState;

  constructor(
    @Inject(APPLICATION_TYPE) public applicationType: "EdgeGateway" | "ServerPool",
    private miscellaneousService: MiscellaneousService,
    private gatewayService: GatewayService,
    private metadataCacheService: MetadataCacheService,
    private snackbar: MatSnackBar
  ) {}

  async ngOnInit() {
    try {
      await firstValueFrom(this.gatewayService.apiGatewayGet());
      this.gatewayState = GatewayState.Running;
    } catch (error: any) {
      this.gatewayState = GatewayState.NotRunning;
    }
  }

  async restartGateway() {
    await firstValueFrom(this.miscellaneousService.apiRestartPost());

    // error handling

    this.gatewayState = GatewayState.Restarting;
    await new Promise(f => setTimeout(f, 5000));
    
    for (var i = 0; i < RETRY_ATTEMPTS; i++) {
      try {
        await firstValueFrom(this.gatewayService.apiGatewayGet());
        this.gatewayState = GatewayState.Running;
        return;
      } catch (error: any) {
        await new Promise(f => setTimeout(f, 2000));
      }
    }

    this.gatewayState = GatewayState.NotRunning;
  }

  clearStoredMetadata() {
    this.metadataCacheService.clearMetadata();
    this.snackbar.open("Metadata storage has been cleared", undefined, { duration: 3000 });
  }
}

const RETRY_ATTEMPTS = 20;

enum GatewayState {
  Running,
  NotRunning,
  Restarting
}
