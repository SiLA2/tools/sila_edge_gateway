import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { firstValueFrom } from 'rxjs';
import { ErrorAreaComponent } from 'src/app/explorer/error-area/error-area.component';
import { DiscoverySpec, GatewayFeatureService, GatewayService, ServerInfo, ServerSpec } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-server-configuration',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    ErrorAreaComponent
  ],
  templateUrl: './server-configuration.component.html',
  styleUrl: './server-configuration.component.css'
})
export class ServerConfigurationComponent implements AfterViewInit {
  serverInfos: ServerInfo[] = [];
  newServerInfos: ServerInfo[] = [];
  hasNewFeatures = false;
  displayedColumns: string[] = ["serverName", "serverType", "serveruuid"];

  discoveryProgress = 0;

  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;
  errorAreaVisible = false;

  discoveryTimeout = new FormControl();
  addServerFormGroup = new FormGroup({
    host: new FormControl(),
    port: new FormControl()
  });

  constructor(
    private gatewayService: GatewayService,
    private gatewayFeatureService: GatewayFeatureService
  ) {}

  async ngAfterViewInit() {
    await this.getConnectedServers();
  }

  async getConnectedServers() {
    this.errorAreaVisible = false;

    try {
      this.serverInfos = await firstValueFrom(this.gatewayService.apiGatewayGet());

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async startServerDiscovery() {
    this.discoveryProgress = 0;

    var discoverySpec: DiscoverySpec = {
      timeout: Math.ceil(this.discoveryTimeout.value)
    };

    this.errorAreaVisible = false;

    try {
      await firstValueFrom(this.gatewayService.apiGatewayPost(discoverySpec));

      var multiplier = 100 / discoverySpec.timeout!;
      for (var i = 0; i < discoverySpec.timeout!; i++) {
        this.discoveryProgress = i * multiplier;
        await new Promise(f => setTimeout(f, 1000));
      }
      this.discoveryProgress = 100;
      
      await this.getNewServers();
      await this.checkForNewFeatures();

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async addServer() {
    this.discoveryProgress = 0;

    var serverSpec: ServerSpec = {
      host: this.addServerFormGroup.get("host")?.value,
      port: this.addServerFormGroup.get("port")?.value
    };

    this.errorAreaVisible = false;

    try {
      await firstValueFrom(this.gatewayService.apiGatewayPut(serverSpec));
      this.discoveryProgress = 100;

      await this.getNewServers();
      await this.checkForNewFeatures();

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async getNewServers() {
    this.errorAreaVisible = false;

    try {
      var serverInfos = await firstValueFrom(this.gatewayService.apiGatewayGet());
      this.newServerInfos = serverInfos.filter(newServerInfo => !this.serverInfos.some(oldServerInfo => newServerInfo.serveruuid == oldServerInfo.serveruuid));
      this.serverInfos = serverInfos;

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async checkForNewFeatures() {
    this.errorAreaVisible = false;

    try {
      var gatewayFeatures = await firstValueFrom(this.gatewayFeatureService.apiGatewayFeaturesGet());
      this.hasNewFeatures = gatewayFeatures.hasNewFeatures!;

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }
}
