import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { firstValueFrom } from 'rxjs';
import { ErrorAreaComponent } from 'src/app/explorer/error-area/error-area.component';
import { ClientConfiguration, ConnectionConfigurationService } from 'src/generated-sources/openapi';

@Component({
  selector: 'app-client-configuration',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatButtonModule,
    ReactiveFormsModule,
    ErrorAreaComponent
  ],
  templateUrl: './client-configuration.component.html',
  styleUrl: './client-configuration.component.css'
})
export class ClientConfigurationComponent implements AfterViewInit {
  clientConfigurations: ClientConfiguration[] = [];
  displayedColumns: string[] = ["clientName", "host", "port"];

  @ViewChild(ErrorAreaComponent) errorAreaComponent!: ErrorAreaComponent;
  errorAreaVisible = false;

  addClientFormGroup = new FormGroup({
    clientName: new FormControl(),
    host: new FormControl(),
    port: new FormControl(),
    persist: new FormControl(true)
  });

  deleteClientFormGroup = new FormGroup({
    clientName: new FormControl(),
    remove: new FormControl(true)
  });

  constructor(
    private connectionConfigurationService: ConnectionConfigurationService
  ) {}

  async ngAfterViewInit() {
    await this.getClientConfigurations();
  }

  async getClientConfigurations() {
    this.errorAreaVisible = false;

    try {
      this.clientConfigurations = await firstValueFrom(this.connectionConfigurationService.apiConnectionConfigurationGet());

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }
  }

  async addClientConfiguration() {
    var clientConfiguration: ClientConfiguration = {
      clientName: this.addClientFormGroup.get("clientName")?.value,
      host: this.addClientFormGroup.get("host")?.value,
      port: this.addClientFormGroup.get("port")?.value
    };

    this.errorAreaVisible = false;

    try {
      await firstValueFrom(this.connectionConfigurationService.apiConnectionConfigurationPut(this.addClientFormGroup.get("persist")?.value!, clientConfiguration));

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }

    await this.getClientConfigurations();
  }

  async deleteClientConfiguration() {
    this.errorAreaVisible = false;

    try {
      await firstValueFrom(this.connectionConfigurationService.apiConnectionConfigurationDelete(this.deleteClientFormGroup.get("clientName")?.value, this.deleteClientFormGroup.get("remove")?.value!));

    } catch(error: any) {
      this.errorAreaVisible = true;
      this.errorAreaComponent.setError(error);
    }

    await this.getClientConfigurations();
  }
}
