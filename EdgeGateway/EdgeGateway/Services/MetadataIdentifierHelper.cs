﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Services
{
    public class MetadataIdentifierHelper : IMetadataIdentifierHelper
    {
        private readonly IGatewayService _gatewayService;

        public MetadataIdentifierHelper(IGatewayService gatewayService)
        {
            _gatewayService = gatewayService;
        }

        public IReadOnlyList<string> GetRequiredMetadata(string serverUuid, string identifier)
        {
            return _gatewayService.GetRequiredMetadata(serverUuid, identifier);
        }
    }
}
