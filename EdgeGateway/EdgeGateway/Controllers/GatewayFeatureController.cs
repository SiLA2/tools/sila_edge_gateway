﻿using Common.Logging;
using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/gatewayFeatures")]
    public class GatewayFeatureController : Controller
    {
        private static readonly string _featuresDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "SiLA2", "EdgeGateway", "Features");
        private readonly ILog _logger = LogManager.GetLogger<GatewayFeatureController>();
        private readonly IFeatureResolver _featureResolver;
        private readonly IGatewayService _gatewayService;

        public GatewayFeatureController(IFeatureResolver featureResolver, IGatewayService gatewayService)
        {
            _featureResolver = featureResolver;
            _gatewayService = gatewayService;

            if (!Directory.Exists(_featuresDir))
            {
                Directory.CreateDirectory(_featuresDir);
            }
        }

        [HttpGet]
        public GatewayFeatures GetGatewayFeatures()
        {
            var features = new List<string>();
            foreach (var file in Directory.GetFiles(_featuresDir))
            {
                try
                {
                    var feature = FeatureSerializer.Load(file);
                    features.Add(feature.FullyQualifiedIdentifier);
                }
                catch (Exception ex)
                {
                    _logger.Error($"Error reading {file}", ex);
                }
            }
            return new GatewayFeatures(features, _gatewayService.HasNewFeature);
        }

        [HttpPut]
        public IActionResult PutFeature(string identifier)
        {
            var feature = _featureResolver.Resolve(identifier);
            if (feature == null)
            {
                return NotFound();
            }
            var featurePath = GetFileName(feature);
            if (!System.IO.File.Exists(featurePath))
            {
                FeatureSerializer.Save(feature, featurePath);
                return Ok();
            }
            return Conflict();
        }

        private static string GetFileName(Feature feature)
        {
            return Path.Combine(_featuresDir, feature.FullyQualifiedIdentifier.Replace('/', '.') + ".sila.xml");
        }

        [HttpDelete]
        public IActionResult DeleteFeature(string identifier)
        {
            var feature = _featureResolver.Resolve(identifier);
            if (feature == null)
            {
                return NotFound();
            }
            var featurePath = GetFileName(feature);
            if (System.IO.File.Exists(featurePath))
            {
                System.IO.File.Delete(featurePath);
                return Ok();
            }
            return NotFound();

        }
    }

    public record GatewayFeatures(List<string> features, bool hasNewFeatures);
}
