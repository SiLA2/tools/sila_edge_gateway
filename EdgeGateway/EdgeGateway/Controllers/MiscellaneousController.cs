﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api")]
    public class MiscellaneousController : Controller
    {
        private readonly IGatewayService _gatewayService;

        public MiscellaneousController(IGatewayService gatewayService)
        {
            _gatewayService = gatewayService;
        }

        [Route("restart")]
        [HttpPost]
        public IActionResult RestartGateway()
        {
            _gatewayService.Restart();
            return Ok();
        }

        [Route("servername")]
        [HttpPost]
        public IActionResult SetServerName([FromBody] ServerNameRequestBody requestBody)
        {
            _gatewayService.SetServerName(requestBody.serverId, requestBody.name);
            return Ok();
        }
        public record ServerNameRequestBody(string serverId, string name);
    }
}
