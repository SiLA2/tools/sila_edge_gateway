# SiLA2 Edge Gateway

This repository contains a generic SiLA2 gateway application and a generic SiLA2 server pool. The gateway is able to perform a server discovery. Afterwards, the gateway is able to expose the union of all features offered by connected
servers both through client-initiated and server-initiated connections. In addition, these features are also exposed through REST endpoints (there is a generated SwaggerUI under /swagger, if you want to try). A corresponding UI is now also included with the gateway and can be used with the server pool as well.
The repository also includes a Server Pool application, that can be targeted by server-initiated connections. 

|||
| ---------------| ----------------------------------------------------------- |
| SiLA Homepage  | [https://sila-standard.com](https://sila-standard.com)      |
| Chat group     | [Join the group on Slack](https://sila-standard.org/slack)|
| Maintainer     | Georg Hinkel ([georg.hinkel@hs-rm.de](mailto:georg.hinkel@hs-rm.de)) of [Rhein-Main University of Applied Sciences](http://www.hs-rm.de)|

# Status

The repository is in a draft state. Currently, there is no installer for any of the applications. Development of the gateway application based on unmanaged gRPC has been stopped, only the version based on managed gRPC is actively being developed.

## Contributing

To get involved, read our [contributing docs](https://gitlab.com/SiLA2/sila_base/blob/master/CONTRIBUTING.md) in [sila_base](https://gitlab.com/SiLA2/sila_base).

### C# Style Guide

Please follow the [general C# coding guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/).
