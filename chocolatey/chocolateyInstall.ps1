$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\Sila Gateway.lnk" -TargetPath "$path\tools\Hsrm.EdgeGateway.exe"